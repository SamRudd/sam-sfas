﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitUI : MonoBehaviour
{
    [Header("UI ELEMENTS")]
    [SerializeField] private Text mUnitName;
    [SerializeField] private Slider mHPSlider;
    [SerializeField] private Slider mAPSlider;

    public Character Unit { get; set; }

    private void Update()
    {
        if (Unit != null)
        {
            // Position & Rotation
            transform.position      = Unit.transform.position + (Vector3.up * 10);
            Vector3 targetPostition = new Vector3(Camera.main.transform.position.x, this.transform.position.y, Camera.main.transform.position.z);

            this.transform.LookAt(targetPostition);
            this.GetComponent<RectTransform>().eulerAngles = new Vector3(-Camera.main.transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);

            // UI
            mUnitName.text     = Unit.name;
            mHPSlider.maxValue = Unit.Starthealth;
            mHPSlider.value    = Unit.health;
            mAPSlider.maxValue = Unit.startAP;
            mAPSlider.value    = Unit.actionPoints;
        }
    }
}
