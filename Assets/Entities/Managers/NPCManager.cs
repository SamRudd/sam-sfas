﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager : TurnManager
{
    [Header("UI")]
    [SerializeField] private Announcements announcements;

    public List<NPC> NPCs       { get; private set; }       // All the NPCs in the game
    public int ExtractedCount   { get; private set; }       // The number of NPCs that got extracted
    public int DeathCount       { get; private set; }       // The number of NPCs that was killed

    private int npcIndex = 0;                   // The current NPCs turn
    private CameraMovement cameraMovement;      // The main camera

    public override void Init()
    {
        // Find all the NPCs spaners and spawn them
        NPCs                = new List<NPC>();
        NPCSpawn[] spawners = Object.FindObjectsOfType<NPCSpawn>();
        cameraMovement              = Camera.main.GetComponent<CameraMovement>();

        foreach (NPCSpawn spawner in spawners)
        {
            NPCs.Add(spawner.Spawn());
            spawner.End();
        }

        // Set the NPC managers
        foreach (NPC npc in NPCs) { npc.manager = this; }
    }

    public override void StartTurn()
    {
        // Reset the npcs
        foreach (NPC npc in NPCs) npc.ResetCharacter();

        // Reset the manager
        npcIndex = 0;
        cameraMovement.CameraReset();
        turnState = TurnState.Active;

        // Start the turn
        TakeNPCTurn();
    }

    public override void UpdateManager()
    {
        // End game if no NPCs remain
        if (NPCs.Count == 0)
        {
            EndTurn();
            GetComponent<Game>().EndGame();
            return;
        }

        // If all units have taken turn then end the managers turn
        if (CheckAllDone())
        {
            EndTurn();
            return;
        }

        // If the current unit has finished their turn start the end turn
        if (NPCs[npcIndex].state == Character.States.TurnTaken && (npcIndex + 1) < NPCs.Count)
        {
            npcIndex++;
            TakeNPCTurn();
        }
    }

    protected override void EndTurn()
    {
        turnState = TurnState.TurnEnded;
    }

    // Destroy, remove and announce death of unit
    public void KillUnit(NPC unit)
    {
        unit.CurrentPosition.playerOcupied = null;
        NPCs.Remove(unit);

        if (unit != null) Destroy(unit.gameObject);

        DeathCount++;
        announcements.NPCDeath();
    }

    // Remove the element
    public void ExtractUnit(NPC unit)
    {
        NPCs.Remove(unit);
        Destroy(unit.gameObject);

        ExtractedCount++;
        TakeNPCTurn();
    }

    public void TakeNPCTurn()
    {
        if (NPCs.Count > 0) StartCoroutine(NPCs[npcIndex].TakeTurn());
    }

    private bool CheckAllDone()
    {
        foreach (NPC npc in NPCs) if (npc.state != Character.States.TurnTaken) return false;

        return true;
    }

    public override bool IsCompleted()
    {
        if (NPCs.Count == 0) return true;

        return false;
    }

    // Not required for NPCs
    public override void FrameManager()
    {
    }
}
