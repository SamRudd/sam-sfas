﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineScript : MonoBehaviour
{
    private LineRenderer line;
    private Vector3 mousePos;
    public Material material;
    private int currLines = 0;

    public Transform pos1, pos2;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (line == null)
            {
                createLine();
            }

            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            line.SetPosition(0, pos1.position);
            line.SetPosition(1, pos2.position);
        }
        //else if (Input.GetMouseButtonUp(0) && line)
        //{
        //    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    mousePos.z = 0;
        //    line.SetPosition(1, mousePos);
        //    line = null;
        //    currLines++;
        //}
        //else if (Input.GetMouseButton(0) && line)
        //{
        //    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //    mousePos.z = 0;
        //    line.SetPosition(1, mousePos);
        //}
    }

    void createLine()
    {
        line = new GameObject("Line" + currLines).AddComponent<LineRenderer>();
        line.material       = material;
        line.positionCount  = 2;
        line.startWidth     = 0.15f;
        line.endWidth       = 0.15f;
        line.useWorldSpace  = true;
        line.numCapVertices = 50;
    }
}
