﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    [Header("MAP DETAILS")]
    [SerializeField] private Vector2Int mapSize = new Vector2Int(20, 20);

    [Header("MOVEMENT")]
    [SerializeField] private GameObject movementTile;           // Tile that shows up during player movement
    [SerializeField] private List<GameObject> movementTiles;    // List of current movement tiles
    [SerializeField] private CharacterManager unitMan;          // Player manager

    [Header("MOVEMENT RAY DRAWING")]
    [SerializeField] private float rayHeight = 5.0f;    // Height from the tile for the ray
    [SerializeField] private float rayWidth  = 0.5f; 
    [SerializeField] private int posCount    = 2;
    [SerializeField] private int capVertices = 50;      // Roundness
    [SerializeField] private Material rayMaterial;      // Colour / properties of ray

    [Header("EXTRACTION")]
    public ExtractionZone extractZone;

    [Header("RANDOM GENERATION")]
    [SerializeField] private LayerMask tileMask;            // Mask for tiles
    [SerializeField] private int visualMin, visualMax;      // The number of objects created on a tile

    [SerializeField] private GameObject[] blockObjects;     // Objects that can be spawned on a blocked tile
    [SerializeField] private GameObject[] visualObjects;    // Objects that can be spawned on a visual tile

    [Range(0.0f, 100.0f)]
    [SerializeField] private float blockChance;     // Chance that a tile will be blocked

    [Range(0.0f, 100.0f)]
    [SerializeField] private float visualChance;    // Chance that a tile will spawn visual objects

    [Header("EDITOR - TILE NAMING")]
    public Transform holder;

    private EnvironmentTile[][]     map;
    private List<EnvironmentTile>   all;
    private List<EnvironmentTile>   toBeTested;
    private List<EnvironmentTile>   lastSolution;

    private const float tileSize   = 10.0f;     // The width / lenght of a tile
    private const float tileHeight = 2.5f;      // Height above tile transform to show on top of tile

    // Getters / Setters
    public EnvironmentTile Start { get; private set; }
    public Vector2Int Size { get { return mapSize; } private set { mapSize = value; } }
    public float TileSize { get { return tileSize; } private set { } }
    public List<EnvironmentTile> ExtractNodes { get; set; }

    private void Awake()
    {
        all           = new List<EnvironmentTile>();
        toBeTested    = new List<EnvironmentTile>();
        movementTiles = new List<GameObject>();
    }

    // Used to give proper names to all the enviroment tiles (Called from Editor button)
    public void NameMap()
    {
        map = new EnvironmentTile[Size.x][];

        for (int x = 0; x < Size.x; ++x)
        {
            GameObject objHolder = new GameObject("Tiles: " + x);
            objHolder.transform.parent = holder;

            for (int y = 0; y < Size.y; ++y)
            {
                Vector3 tilePos = new Vector3((x * TileSize + (TileSize / 2.0f)), 0.0f, (y * TileSize + (TileSize / 2.0f)));

                Collider[] hitColliders = Physics.OverlapSphere(tilePos, 1.0f, tileMask);
                hitColliders[0].transform.name = "Tile: " + x + " | " + y;
                hitColliders[0].transform.parent = objHolder.transform;
            }
        }
    }

    private void Generate()
    {
        ExtractNodes = new List<EnvironmentTile>();
        map          = new EnvironmentTile[Size.x][];

        bool start     = true;
        int halfWidth  = Size.x / 2;
        int halfHeight = Size.y / 2;
       
        for (int x = 0; x < Size.x; ++x)
        {
            map[x] = new EnvironmentTile[Size.y];

            for (int y = 0; y < Size.y; ++y)
            {
                Vector3 tilePos = new Vector3((x * TileSize + (TileSize / 2.0f)), 0.0f, (y * TileSize + (TileSize / 2.0f)));

                // Get the tile in this position
                Collider[] hitColliders = Physics.OverlapSphere(tilePos, 1.0f, tileMask);
                map[x][y] = hitColliders[0].GetComponent<EnvironmentTile>();

                // Set up the details for the tile
                map[x][y].Position         = tilePos + new Vector3(0.0f, tileHeight * map[x][y].transform.localScale.y, 0.0f);
                map[x][y].IsAccessible     = true;
                map[x][y].gameObject.name  = string.Format("Tile({0},{1})", x, y);
                map[x][y].Index            = new Vector2Int(x, y);
                map[x][y].playerOcupied    = null;
                map[x][y].SetupBoundrys();

                // Generate random vegitation and rocks
                if (map[x][y].UseRandomness)
                {
                    // Generate chance of blocking and visual (if blocking occurs visual is forced to occur)
                    float blockRand     = Random.Range(0.0f, 100.0f);
                    float visualRand    = Random.Range(0.0f, 100.0f);

                    if (blockRand <= blockChance)
                    {
                        // Create Hold object
                        GameObject parent = new GameObject("Block");
                        parent.transform.parent = map[x][y].transform;

                        // Get random position within the tile and a random rotation
                        Vector3 posOffet = new Vector3(Random.Range(-1.0f, 1.0f), -1.5f, Random.Range(-1.0f, 1.0f));
                        Vector3     pos  = map[x][y].Position + (Vector3.up * map[x][y].transform.localScale.y) + posOffet;
                        Quaternion  rot  = Quaternion.Euler(-90.0f, Random.Range(0.0f, 360.0f), 0.0f);

                        // Create a random blocking object
                        Instantiate(blockObjects[Random.Range(0,  blockObjects.Length - 1)], pos, rot, parent.transform);

                        // Tile is blocked so make it so units can acces it
                        map[x][y].IsAccessible = false;

                        // Force visual generation
                        visualRand = 0.0f;
                    }

                    if (visualRand <= visualChance)
                    {
                        // Number of objects to be spawned
                        int objCount = Random.Range(visualMin, visualMax);

                        // Holder for objects
                        GameObject parent = new GameObject("Visual");
                        parent.transform.parent = map[x][y].transform;

                        for (int i = 0; i < objCount; i++)
                        {
                            // Get random position within the tile and a random rotation and scale
                            Vector3 posOffet        = new Vector3(Random.Range(-4.0f, 4.0f), -1.5f, Random.Range(-4.0f, 4.0f));
                            Vector3     pos         = map[x][y].Position + (Vector3.up * map[x][y].transform.localScale.y) + posOffet;
                            Quaternion  rot         = Quaternion.Euler(-90.0f, Random.Range(0.0f, 360.0f), 0.0f);
                            float scl               = Random.Range(1.0f, 3.0f);

                            // Create a random visual object
                            GameObject obj = Instantiate(visualObjects[Random.Range(0, visualObjects.Length - 1)], pos, rot, parent.transform);
                            obj.transform.localScale = obj.transform.localScale * scl;
                        }
                    }
                }

                map[x][y].MergeMeshes();

                if (map[x][y].CanExtract) ExtractNodes.Add(map[x][y]);

                all.Add(map[x][y]);

                if (start) Start = map[x][y];

                start = false;
            }
        }
    }

    // Loopp through each tile and set its neighbours
    public void SetupConnections()
    {
        // Loopp through each tile and setup the connections for each tile
        for (int x = 0; x < Size.x; ++x)
        {
            for (int y = 0; y < Size.y; ++y)
            {
                EnvironmentTile tile = map[x][y];
                tile.Connections     = new List<EnvironmentTile>();

                if (tile.TileBlocked) continue;

                // Check each side of the tile connection to see if either side has a boundry blocking the connection

                // Left
                if (x > 0 && !map[x][y].BoundryLeft && !map[x - 1][y].BoundryRight && !map[x - 1][y].TileBlocked)
                {
                    if (map[x - 1][y].transform.localScale.y == map[x][y].transform.localScale.y ||
                        map[x - 1][y].transform.localScale.y == map[x][y].transform.localScale.y + 2 ||
                        map[x - 1][y].transform.localScale.y == map[x][y].transform.localScale.y - 2)
                    {
                        tile.Connections.Add(map[x - 1][y]);
                    }
                }

                // Right
                if (x < Size.x - 1 && !map[x][y].BoundryRight && !map[x + 1][y].BoundryLeft && !map[x + 1][y].TileBlocked)
                {
                    if (map[x + 1][y].transform.localScale.y == map[x][y].transform.localScale.y ||
                        map[x + 1][y].transform.localScale.y == map[x][y].transform.localScale.y + 2 ||
                        map[x + 1][y].transform.localScale.y == map[x][y].transform.localScale.y - 2)
                    {
                        tile.Connections.Add(map[x + 1][y]);
                    }
                }

                // Down
                if (y > 0 && !map[x][y].BoundryDown && !map[x][y - 1].BounryUp && !map[x][y - 1].TileBlocked)
                {
                    if (map[x][y - 1].transform.localScale.y == map[x][y].transform.localScale.y ||
                        map[x][y - 1].transform.localScale.y == map[x][y].transform.localScale.y + 2 ||
                        map[x][y - 1].transform.localScale.y == map[x][y].transform.localScale.y - 2)
                    {
                        tile.Connections.Add(map[x][y - 1]);
                    }
                }

                // Forward
                if (y < Size.y - 1 && !map[x][y].BounryUp && !map[x][y + 1].BoundryDown && !map[x][y + 1].TileBlocked)
                {
                    if (map[x][y + 1].transform.localScale.y == map[x][y].transform.localScale.y ||
                        map[x][y + 1].transform.localScale.y == map[x][y].transform.localScale.y + 2 ||
                        map[x][y + 1].transform.localScale.y == map[x][y].transform.localScale.y - 2)
                    {
                        tile.Connections.Add(map[x][y + 1]);
                    }
                }
            }
        }
    }

    // If the index is within bounds return back the tile in that position
    public EnvironmentTile GetTile(int x, int y)
    {
        if (x >= Size.x || y >= Size.y) Debug.Log(x + " " + y);

        return map[x][y];
    }

    // Draw ray from the current players position to the passed tile
    public void DrawPath(EnvironmentTile end)
    {
        // Find the path
        List<EnvironmentTile> path = Solve(unitMan.GetActiveUnit().CurrentPosition, end);
        List<LineRenderer> lines   = new List<LineRenderer>();

        // Draw line between each tile in the path
        int i = 0;
        foreach (EnvironmentTile tile in path)
        {
            lines.Add(CreateLine(tile.Index.x, tile.Index.y));
            if (lines.Count > 1)
            {
                lines[i].SetPosition(0, lines[i].transform.position);
                lines[i].SetPosition(1, lines[i - 1].transform.position);
            }
            i++;
        }
    }

    // Create a new gameobject on the tile of the index passed and set up the line renderer properties
    private LineRenderer CreateLine(int x, int y)
    {
        LineRenderer line       = new GameObject("Line").AddComponent<LineRenderer>();
        line.transform.position = map[x][y].Position + (Vector3.up * rayHeight);
        line.transform.parent   = transform;

        line.material       = rayMaterial;
        line.positionCount  = posCount;
        line.startWidth     = rayWidth;
        line.endWidth       = rayWidth;
        line.useWorldSpace  = true;
        line.numCapVertices = capVertices;

        return line;
    }

    public void DeletePath()
    {
        // deltete current path
        LineRenderer[] lr = GetComponentsInChildren<LineRenderer>();

        for (int l = 0; l < lr.Length; l++) Destroy(lr[l].gameObject);
    }

    // Create movement tiles on the Enviroment tiles that the player unit can move to
    public void ShowMoveMaterial(EnvironmentTile start, int radius, bool reset)
    {
        // Remove old times
        if (reset) DeletePath();

        for (int i = 0; i < movementTiles.Count; i++) Destroy(movementTiles[i]);
        movementTiles.Clear();

        // Loop through all the movement tiles in the radius of the players capible movement and check if the tile can be reached if so create a movement tile there
        for (int x = start.Index.x - radius + 1; x < start.Index.x + radius; x++)
        {
            for (int y = start.Index.y - radius + 1; y < start.Index.y + radius; y++)
            {
                if (x >= 0 && y >= 0 && x < Size.x && y < Size.y)
                {
                    if (map[x][y].IsAccessible)
                    {
                        if (reset)
                        {
                            map[x][y].PlayerCanReach = false;
                        }
                        else
                        {
                            // Create movement tile and set the tile so that the player can move there
                            if (!map[x][y].playerOcupied)
                            {
                                List<EnvironmentTile> t = Solve(start, map[x][y]);
                                if (t != null && t.Count <= radius)
                                {
                                    GameObject obj = Instantiate(movementTile, map[x][y].Position + (Vector3.up * map[x][y].transform.localScale.y), Quaternion.identity, transform);
                                    obj.GetComponent<TileSelection>().environment = this;
                                    obj.GetComponent<TileSelection>().tile = map[x][y];

                                    movementTiles.Add(obj);
                                    map[x][y].PlayerCanReach = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public float Distance(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the length of the connection between these two nodes to find the distance, this 
        // is used to calculate the local goal during the search for a path to a location
        float result = float.MaxValue;
        EnvironmentTile directConnection = a.Connections.Find(c => c == b);
        if (directConnection != null)
        {
            result = tileSize;
        }
        return result;
    }

    // Return the number of tiles that need to be taken to reach a tile
    public int StepDistance(EnvironmentTile a, EnvironmentTile b)
    {
        List<EnvironmentTile> rs = Solve(a, b);

        if (rs != null) return rs.Count;

        return 0;
    }

    private float Heuristic(EnvironmentTile a, EnvironmentTile b)
    {
        // Use the locations of the node to estimate how close they are by line of sight
        // experiment here with better ways of estimating the distance. This is used  to
        // calculate the global goal and work out the best order to prossess nodes in
        return Vector3.Distance(a.Position, b.Position);
    }

    // Generate the randomness for the tiles and setup their connections
    public void GenerateWorld()
    {
        Generate();
        SetupConnections();
        extractZone.Init();
    }

    public void CleanUpWorld()
    {
        if (map != null)
        {
            for (int x = 0; x < Size.x; ++x)
            {
                for (int y = 0; y < Size.y; ++y)
                {
                    Destroy(map[x][y].gameObject);
                }
            }
        }
    }

    public List<EnvironmentTile> Solve(EnvironmentTile begin, EnvironmentTile destination)
    {
        List<EnvironmentTile> result = null;
        if (begin != null && destination != null)
        {
            // Nothing to solve if there is a direct connection between these two locations
            EnvironmentTile directConnection = begin.Connections.Find(c => c == destination);
            if (directConnection == null)
            {
                // Set all the state to its starting values
                toBeTested.Clear();

                for( int count = 0; count < all.Count; ++count )
                {
                    all[count].Parent  = null;
                    all[count].Global  = float.MaxValue;
                    all[count].Local   = float.MaxValue;
                    all[count].Visited = false;
                }

                // Setup the start node to be zero away from start and estimate distance to Target
                EnvironmentTile currentNode = begin;
                currentNode.Local           = 0.0f;
                currentNode.Global          = Heuristic(begin, destination);

                // Maintain a list of nodes to be tested and begin with the start node, keep going
                // as long as we still have nodes to test and we haven't reached the destination
                toBeTested.Add(currentNode);

                while (toBeTested.Count > 0 && currentNode != destination)
                {
                    // Begin by sorting the list each time by the heuristic
                    toBeTested.Sort((a, b) => (int)(a.Global - b.Global));

                    // Remove any tiles that have already been visited
                    toBeTested.RemoveAll(n => n.Visited);

                    // Check that we still have locations to visit
                    if (toBeTested.Count > 0)
                    {
                        // Mark this note visited and then process it
                        currentNode = toBeTested[0];
                        currentNode.Visited = true;

                        // Check each neighbour, if it is accessible and hasn't already been 
                        // processed then add it to the list to be tested 
                        for (int count = 0; count < currentNode.Connections.Count; ++count)
                        {
                            EnvironmentTile neighbour = currentNode.Connections[count];

                            //bool neighbourOc = (neighbour.playerOcupied != null);

                            if (!neighbour.Visited && neighbour.IsAccessible)
                            {
                                toBeTested.Add(neighbour);
                            }

                            // Calculate the local goal of this location from our current location and 
                            // test if it is lower than the local goal it currently holds, if so then
                            // we can update it to be owned by the current node instead 
                            float possibleLocalGoal = currentNode.Local + Distance(currentNode, neighbour);
                            if (possibleLocalGoal < neighbour.Local)
                            {
                                neighbour.Parent = currentNode;
                                neighbour.Local  = possibleLocalGoal;
                                neighbour.Global = neighbour.Local + Heuristic(neighbour, destination);
                            }
                        }
                    }
                }

                // Build path if we found one, by checking if the destination was visited, if so then 
                // we have a solution, trace it back through the parents and return the reverse route
                if (destination.Visited)
                {
                    result = new List<EnvironmentTile>();
                    EnvironmentTile routeNode = destination;

                    while (routeNode.Parent != null)
                    {
                        result.Add(routeNode);
                        routeNode = routeNode.Parent;
                    }
                    result.Add(routeNode);
                    result.Reverse();

                    //Debug.LogFormat("Path Found: {0} steps {1} long", result.Count, destination.Local);
                }
                else
                {
                    Debug.LogWarning("Path Not Found");
                }
            }
            else
            {
                result = new List<EnvironmentTile>();
                result.Add(begin);
                result.Add(destination);
                //Debug.LogFormat("Direct Connection: {0} <-> {1} {2} long", begin, destination, TileSize);
            }
        }
        else
        {
            Debug.LogWarning("Cannot find path for invalid nodes");
        }

        lastSolution = result;

        return result;
    }
}
