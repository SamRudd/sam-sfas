﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HotkeyManager : MonoBehaviour
{
    [System.Serializable]
    public struct Hotkey
    {
        public KeyCode      key;            // Hotkey
        public UnityEvent   action;         // Function to be called when key is pressed
    }

    [SerializeField] private Hotkey[] hotkeys;              
    [SerializeField] private MusicManager musicManager;     // Used to play button sound when key is pressed

    private bool init = false;  // Has the game started

    public void Init()
    {
        init = true;
    }

    private void Update()
    {
        if (init)
        {
            // If the hotkey key is pressed then invoke the action and play sound
            foreach (Hotkey hotkey in hotkeys)
            {
                if (Input.GetKeyDown(hotkey.key))
                {
                    musicManager.PlayButton();
                    hotkey.action.Invoke();
                }
            }
        }
    }
}
