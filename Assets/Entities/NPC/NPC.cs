﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Character
{
    public GameObject hidingObj, escapingObj;

    public enum NPCState { Hiding, Escaping, Dead, }
    public NPCState npcState;

    public Environment mMap;
    public NPCManager manager;

    private void Awake()
    {
        hidingObj.SetActive((npcState == NPCState.Hiding));
        escapingObj.SetActive((npcState == NPCState.Escaping));
    }

    private void Update()
    {
        //Vector3 targetPostition = new Vector3(Camera.main.transform.position.x, this.transform.position.y, Camera.main.transform.position.z);
        //this.transform.LookAt(targetPostition);
        hidingObj.transform.LookAt((new Vector3(Camera.main.transform.position.x, hidingObj.transform.position.y, Camera.main.transform.position.z)));
        escapingObj.transform.LookAt((new Vector3(Camera.main.transform.position.x, hidingObj.transform.position.y, Camera.main.transform.position.z)));

    }

    public IEnumerator TakeTurn()
    {
        state = Character.States.Active;
        Camera.main.GetComponent<CameraMovement>().Target = transform;

        if (npcState == NPCState.Escaping)
        {
            while (actionPoints > 0)
            {
                if (state == Character.States.Active)
                {
                    if (state == Character.States.Active)
                    {
                        if (CurrentPosition.CanExtract)
                        {
                            // Extract
                            state = States.TurnTaken;

                            manager.ExtractUnit(this);
                            yield break;
                        }
                        else
                        {
                            EnvironmentTile pair = FindClosest();
                            int step = mMap.StepDistance(CurrentPosition, pair);
                            List<EnvironmentTile> route = mMap.Solve(CurrentPosition, pair);

                            if (pair == null)
                            {
                                state = States.TurnTaken;
                                actionPoints = 0;
                                yield break;

                            }

                            if (route != null)
                            {
                                CurrentPosition.playerOcupied = null;

                                if (route.Count <= movementPerAP)
                                {
                                    GoTo(route);
                                    route[route.Count - 1].playerOcupied = this;
                                    CurrentPosition = route[route.Count - 1];
                                }
                                else
                                {
                                    List<EnvironmentTile> partPath = FindPartPath(pair);
                                    GoTo(partPath);

                                    partPath[partPath.Count - 1].playerOcupied = this;
                                    CurrentPosition = route[partPath.Count - 1];
                                }
                            }
                            else
                            {
                                state = States.TurnTaken;
                                actionPoints = 0;
                                yield break;
                            }


                        }
                    }
                }
                yield return 0;
            }
        }

        state = Character.States.TurnTaken;
        yield break;
    }

    protected virtual EnvironmentTile FindClosest()
    {
        EnvironmentTile closest = null;
        EnvironmentTile closestRaw = null;

        int count               = 999;
        int countRaw = 999;

        foreach (EnvironmentTile t in mMap.ExtractNodes)
        {
            if (t.playerOcupied) continue;
            
            List<EnvironmentTile> routeCharater = mMap.Solve(CurrentPosition, t);

            if (routeCharater == null) routeCharater = new List<EnvironmentTile>();

            if (countRaw < routeCharater.Count)
            {
                countRaw = routeCharater.Count;
                closestRaw = t;
            }

            if (routeCharater.Count < count)
            {
                int tilecCount = 999;
                    
                int stepCount = mMap.StepDistance(CurrentPosition, t);
                if (stepCount < tilecCount)
                {
                    closest     = t;
                    tilecCount  = stepCount;
                    count       = routeCharater.Count;

                    if (stepCount == 0) return closest;
                }
            }
        }

        //if (closest == null)
        //{
        //    foreach (EnvironmentTile t in mMap.extractNodes)
        //    {
        //        List<EnvironmentTile> routeCharater = mMap.Solve(CurrentPosition, t);

        //        if (routeCharater.Count < countRaw)
        //        {
        //            countRaw = routeCharater.Count;
        //            closestRaw = t;
        //        }
        //    }

        //    if (CurrentPosition == null) Debug.Log("Current");
        //    if (closestRaw == null) Debug.Log("Raw");

        //    List<EnvironmentTile> path = FindPartPathX(closestRaw);

        //    closest = path[path.Count - 1];
        //}

        //if (closest == null) Debug.LogError("NO VALID MOVEMENT TILE");

        return closest;
    }

    public List<EnvironmentTile> FindPartPathX(EnvironmentTile target)
    {
        // Write for Zombies
        List<EnvironmentTile> route = mMap.Solve(CurrentPosition, target);
        List<EnvironmentTile> partPath = new List<EnvironmentTile>();

        for (int i = 0; i < route.Count; i++)
        {
            if (route[i].playerOcupied == null) partPath.Add(route[i]);
            else break;
        }

        return partPath;
    }

    public List<EnvironmentTile> FindPartPath(EnvironmentTile target)
    {
        List<EnvironmentTile> route = mMap.Solve(CurrentPosition, target);
        List<EnvironmentTile> newRoute = mMap.Solve(CurrentPosition, route[movementPerAP - 1]);
        List<EnvironmentTile> partPath = new List<EnvironmentTile>();

        for (int i = 0; i < newRoute.Count; i++)
        {
            if (newRoute[i].playerOcupied == null) partPath.Add(newRoute[i]);
            else break;
        }

        return partPath;
    }

    public override void TakeDamage(int amount, Character dealer)
    {

        if (hitNoise != null) hitNoise.Play();

        health -= amount;

        if (health <= 0)
        {
            AIControlledCharacter attacker = dealer as AIControlledCharacter;
            attacker.AIPack.SightedNPCs.Remove(this);
            state = States.Dead;
            if (hitNoise != null) deathNoise.Play();
            manager.KillUnit(this);
        }
    }

    public void Activate()
    {
        npcState = NPCState.Escaping;

        hidingObj.SetActive(false);
        escapingObj.SetActive(true);
    }

}
