﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public TileEntity placed;
    public TileEntity[] surround;

    public TileManager tm;

    public int x, y;

    public enum Transistions { RightToLeft, LeftToRight, TopToBottom, BottomToTop, }

    private void Awake()
    {
        surround = new TileEntity[4];
    }

    private void OnMouseDown()
    {
        tm.currentTile = this;
    }

    private void OnMouseOver()
    {
        tm.currentTile = this;

    }
}
