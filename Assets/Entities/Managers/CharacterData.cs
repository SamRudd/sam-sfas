﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Character/CharacterData", order = 1)]
public class CharacterData : ScriptableObject
{
    [Header("TYPE")]
    public Sprite typeIcon;

    [Header("NEGATIVE TRAIT")]
    public Sprite   negativeIcon;
    public string   negativeName;
    public string   negativeDescription;

    [Header("POSITIVE TRAIT")]
    public Sprite  positiveIcon;
    public string  positiveName;
    public string  positiveDescription;

    [Header("ABILITY")]
    public Sprite  abilityIcon;
    public string  abilityName;
    public string  abilityDescription;
    public int     abilityAPCost = 2;       // The AP cost to do ability
}
