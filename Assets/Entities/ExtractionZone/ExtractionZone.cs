﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtractionZone : MonoBehaviour
{
    public enum ExtractState { Ready, Placing, OneTurnAway, TwoTurnAway, }

    [Header("State")]
    [SerializeField] private ExtractState state;

    [Header("MANAGERS")]
    [SerializeField] private Game game;
    [SerializeField] private CharacterManager characterManger;
    [SerializeField] private Environment mMap;
    [SerializeField] private EnvironmentTile start;

    [Header("PLACEMENT")]
    [SerializeField] private LayerMask tileMask;                                //  Raycasting for the tile
    [SerializeField] private Material CanBePlaced, CannotbePlaced;
    [SerializeField] private Vector3 offset = new Vector3(4.5f, -0.5f, 4.5f);   // Where to place in relation to the start tile
    [SerializeField] private int placementDist = 3;

    [Header("EXTRACTION ZONE OBJECTS")]
    [SerializeField] private Transform extractionZoneOutline;
    [SerializeField] private Transform one, two;
    [SerializeField] private GameObject lights;

    private int turn = -999;
    private EnvironmentTile[] tiles;
    private EnvironmentTile targetTile;

    public void Init()
    {
        PlaceExtract(start);
    }

    public void OnBeginPlacing()
    {
        // If already placeing then stop placeing
        if (state == ExtractState.Placing) { Cancel(); return; }

        state = ExtractState.Placing;

        ExtractZoneShow(true);
        two.gameObject.SetActive(false);
        one.gameObject.SetActive(false);
    }

    // Hide the outline and show the lights
    public void PlaceExtract(EnvironmentTile tile)
    {
        state = ExtractState.Ready;

        two.gameObject.SetActive(false);
        one.gameObject.SetActive(false);
        ExtractZoneShow(false);

        ExtractZoneLightsShow(true);

        GetNewTiles(tile);
        EnviromentUpdate();
    }

    public void BeginExtractPlace(EnvironmentTile tile)
    {
        state      = ExtractState.TwoTurnAway;
        targetTile = tile;
    }

    // Cancel the new extraction zone process
    public void Cancel()
    {
        ExtractZoneShow(false);
        two.gameObject.SetActive(false);
        one.gameObject.SetActive(false);
        state = ExtractState.Ready;
    }

    private void Update()
    {
        if (state == ExtractState.Placing)
        {
            // Cancel Placement
            if (Input.GetMouseButtonDown(1)) Cancel();

            // Get tile
            Ray screenClick = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hs = Physics.RaycastAll(screenClick, Mathf.Infinity, tileMask);

            if (hs.Length > 0)
            {
                EnvironmentTile tile = null;
                foreach (RaycastHit h in hs)
                {
                    if (h.transform.GetComponent<EnvironmentTile>() != null)
                    {
                        tile = h.transform.GetComponent<EnvironmentTile>();
                        break;
                    }
                }

                if (tile != null)
                {
                    transform.position = tile.Position + (Vector3.up * tile.transform.localScale.y) + offset;

                    if (CanPlace(tile) && Input.GetMouseButtonDown(0))
                    {
                        turn = game.Turn;
                        BeginExtractPlace(tile);
                    }
                }
            }
        }

        else if (state == ExtractState.TwoTurnAway)
        {
            two.gameObject.SetActive(true);
            one.gameObject.SetActive(false);

            if (game.Turn == turn + 1) state = ExtractState.OneTurnAway;
        }

        else if (state == ExtractState.OneTurnAway)
        {
            two.gameObject.SetActive(false);
            one.gameObject.SetActive(true);

            if (game.Turn == turn + 2) PlaceExtract(targetTile);
        }
    }

    // Check that all the tiles are the same hight and not blocked
    public bool CanPlace(EnvironmentTile t0)
    {
        float y        = t0.transform.localScale.y;
        Vector2Int pos = characterManger.GetActiveUnit().CurrentPosition.Index;

        if (!(t0.Index.x + 1 < mMap.Size.x && t0.Index.y + 1 < mMap.Size.y)) { extractionZoneOutline.GetComponent<MeshRenderer>().material = CannotbePlaced; return false; }

        if (!(t0.Index.x < (pos.x + placementDist) && t0.Index.x > (pos.x - placementDist) &&
              t0.Index.y < (pos.y + placementDist) && t0.Index.y > (pos.y - placementDist)))
        { extractionZoneOutline.GetComponent<MeshRenderer>().material = CannotbePlaced; return false; }

        EnvironmentTile t1 = mMap.GetTile(t0.Index.x + 1,   t0.Index.y);
        EnvironmentTile t2 = mMap.GetTile(t0.Index.x + 1,   t0.Index.y + 1);
        EnvironmentTile t3 = mMap.GetTile(t0.Index.x,       t0.Index.y + 1);

        if (!t0.IsAccessible) return false;
        if (!t1.IsAccessible || t1.transform.localScale.y != y) { extractionZoneOutline.GetComponent<MeshRenderer>().material = CannotbePlaced; return false; }
        if (!t2.IsAccessible || t2.transform.localScale.y != y) { extractionZoneOutline.GetComponent<MeshRenderer>().material = CannotbePlaced; return false; }
        if (!t3.IsAccessible || t3.transform.localScale.y != y) { extractionZoneOutline.GetComponent<MeshRenderer>().material = CannotbePlaced; return false; }

        extractionZoneOutline.GetComponent<MeshRenderer>().material = CanBePlaced;

        return true;
    }

    public void ExtractZoneLightsShow(bool state)
    {
        lights.transform.position = transform.position;
        foreach (Light light in lights.GetComponentsInChildren<Light>()) light.enabled = state;
    }

    public void ExtractZoneShow(bool state)
    {
        extractionZoneOutline.GetComponent<MeshRenderer>().enabled = state;
    }

    // Get all the tiles that will fall in the extraction zone
    private void GetNewTiles(EnvironmentTile tile)
    {
        tiles = new EnvironmentTile[4];

        tiles[0] = tile;
        tiles[1] = mMap.GetTile(tile.Index.x + 1,   tile.Index.y);
        tiles[2] = mMap.GetTile(tile.Index.x + 1,   tile.Index.y + 1);
        tiles[3] = mMap.GetTile(tile.Index.x,       tile.Index.y + 1);
    }

    // Remove old extraction zone and set up a new one
    private void EnviromentUpdate()
    {
        // Remove Extraction ability from old tiles
        foreach (EnvironmentTile t in mMap.ExtractNodes) t.CanExtract = false;
        
        // Remove old tiles
        mMap.ExtractNodes.Clear();
        mMap.ExtractNodes = new List<EnvironmentTile>();

        // Add new Tiles
        mMap.ExtractNodes.Add(tiles[0]);
        mMap.ExtractNodes.Add(tiles[1]);
        mMap.ExtractNodes.Add(tiles[2]);
        mMap.ExtractNodes.Add(tiles[3]);

        // Add Extraction ability to new tiles
        foreach (EnvironmentTile t in mMap.ExtractNodes) t.CanExtract = true;       
    }
}
