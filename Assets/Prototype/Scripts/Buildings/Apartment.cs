﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apartment : Building
{
    public enum ApartmentQuailty { Cheap, Fine, Good, Luxury, }

    [Header("Apartment")]
    public ApartmentQuailty quailty;

    StaffManager sm;

    public override void BuildingInit()
    {
        sm = GameObject.Find("GameManager").transform.Find("StaffManager").GetComponent<StaffManager>();
        sm.staffLivingSpace += 10;
    }

    public override void BuildingDestroy()
    {
        sm.staffLivingSpace -= 10;
    }

    public override void BuildingUpdate()
    {
        
    }
}
