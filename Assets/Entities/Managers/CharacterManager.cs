﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterManager : TurnManager
{
    [Header("UNIT PREFABS")]
    // Handle player controlled agents
    [SerializeField] private Soldier    soldierPrefab;
    [SerializeField] private Scout      scoutPrefab;

    private const int numOfCharacters = 4;

    [Header("MANAGERS")]
    [SerializeField] private CharacterSetup setupManger;
    [SerializeField] private Environment mMap;
    private CharacterMovement cm;

    [Header("UI")]
    public CharacterUIHandler characterUI;

    private PlayerControledCharacter SelectedUnit;  // The currently selected unit
    private int unitIndex = 0;                      // The index of the unit currently selected

    private List<PlayerControledCharacter> mUnits;              // Holds all alive units
    private List<PlayerControledCharacter> mDeadUnits;          // Holds all dead units for stats at the end
    private List<PlayerControledCharacter> mExtractedUnits;     // Holds all extracted units for stats at the end

    private bool turnInProgress     = false;        // Is the current unit performing an action
    private bool turnInProgressold  = false;        // Was the unit performing an action last frame

    public bool CritMultipler { get; set; } = false;    // should a crit multipler be applied to attacks

    private bool attackUI = false;          // Show attack UI
    private bool lookingAround = false;     // Is the Unit looking around

    public bool AllUnitsDead() { if (GetDeadUnits() == numOfCharacters) return true; else return false; }

    public override void FrameManager()
    {
        // Foreach pack that is hidden check if the unit can see any of the units
        foreach (Pack pack in GetComponent<AIManager>().Packs)
        {
            if (pack.visability == Pack.PackVisabilityStatus.Hidden)
            {
                foreach(Character character in pack.Units)
                {
                    if (character.IsVisibleFrom(GetActiveUnit().cam))
                    {
                        StartCoroutine(pack.SetVisible(character));
                    }
                }
            }
        }

    }

    public int GetDeadUnits()
    {
        return numOfCharacters - (mUnits.Count + mExtractedUnits.Count);
    }

    // Check all player units for any that have lost health
    public int GetWoundedUnits()
    {
        int count = 0;

        foreach (Character character in mUnits)          if (character.health < character.Starthealth) count++;
        foreach (Character character in mExtractedUnits) if (character.health < character.Starthealth) count++;

        return count;
    }

    // Is there any units remaining
    public override bool IsCompleted()
    {
        if (mUnits.Count == 0) return true;

        return false;
    }

    public List<Character> GetUnits()
    {
        List<Character> un = new List<Character>();
        foreach (PlayerControledCharacter pcc in mUnits) un.Add(pcc);

        return un;
    }

    public override void Init()
    {
        // Init movement
        cm = GetComponent<CharacterMovement>();
        cm.ControlInit();

        // Create lists
        mUnits          = new List<PlayerControledCharacter>();
        mDeadUnits      = new List<PlayerControledCharacter>();
        mExtractedUnits = new List<PlayerControledCharacter>();

        setupManger.GetCharacters();

        PlayerControledCharacter mCharacter = null;
        GameObject ui;

        for (int i = 0; i < numOfCharacters; i++)
        {

            // Find the unit type and create it
            if      (setupManger.setup[i].Value.Equals("Soldier"))  mCharacter = Instantiate(soldierPrefab, transform);
            else if (setupManger.setup[i].Value.Equals("Scout"))    mCharacter = Instantiate(scoutPrefab, transform);
            else                                                    mCharacter = Instantiate(soldierPrefab, transform);

            ui         = Instantiate(characterUI.mUnitUI, characterUI.content);

            // Makes the characters spawn in a square
            EnvironmentTile spawnTile = mMap.GetTile((i & 2) / 2, (i % 2));

            // Setup details for unit
            mCharacter.transform.position   = spawnTile.Position;
            mCharacter.transform.rotation   = Quaternion.identity;
            mCharacter.CurrentPosition      = spawnTile;
            mCharacter.transform.name       = setupManger.setup[i].Key;
            mCharacter.manager              = this;
            mCharacter.UIobj                = ui;
            mCharacter.cam                  = mCharacter.GetComponentInChildren<Camera>();
            mCharacter.cam.targetDisplay    = i + 1;
            mCharacter.cam.targetTexture    = new RenderTexture(512, 288, 8);

            spawnTile.playerOcupied         = mCharacter;

            ui.GetComponent<UnitUI>().Unit = mCharacter;

            SetActiveCharacter(mCharacter);
            mUnits.Add(mCharacter);
            characterUI.playerCameraView.TargetChanged();
        }
    }

    public override void StartTurn()
    {
        // End game if out of units
        if (mUnits.Count == 0) { turnState = TurnState.TurnEnded; GetComponent<Game>().EndGame(); return; } 

        // Reset units and UI
        SetActiveCharacter(mUnits[0]);
        Camera.main.GetComponent<CameraMovement>().Target = SelectedUnit.transform;
        characterUI.UI.SetActive(true);
        foreach (Character character in mUnits) character.ResetCharacter();
        turnState = TurnState.Active;
        UpdateUI();
    }

    public void KillUnit(PlayerControledCharacter unit)
    {
        unit.CurrentPosition.playerOcupied = null;
        
        // Create a zombie in the position of dead unit
        GetComponent<AIManager>().Packs[GetComponent<AIManager>().PackIndex].AddUnit(unit.CurrentPosition.Index.x, unit.CurrentPosition.Index.y);

        mDeadUnits.Add(unit);
        mUnits.Remove(unit);

        // End game if out of units
        if (mUnits.Count == 0) { turnState = TurnState.TurnEnded; GetComponent<Game>().EndGame(); return; }

        if (SelectedUnit.Equals(unit)) SetActiveCharacter(mUnits[0]);
        unit.UIobj.SetActive(false);
        unit.gameObject.SetActive(false);

        UpdateUI();
    }

    // End the turn of the manager
    protected override void EndTurn()
    {
        characterUI.UI.SetActive(false);
        turnState = TurnState.TurnEnded;
    }

    public override void UpdateManager()
    {
        // Update movement
        cm.ControlUpdate();

        // Manager turn over
        if (CheckAllDone()) { EndTurn(); return; }

        // Check if action has ended
        if  (SelectedUnit.state == Character.States.TurnInProgress) { characterUI.UI.SetActive(false); turnInProgress = true; }
        else                                                        { characterUI.UI.SetActive(true); turnInProgress = false; }

        // If action ended then update UI
        if (turnInProgress != turnInProgressold) UpdateUI();

        turnInProgressold = turnInProgress;

        // If out of action then go to next unit
        if (SelectedUnit.actionPoints == 0) NextUnit();

        for (int i = 0; i < mUnits.Count; i++) if (mUnits[i].state == Character.States.Dead) KillUnit(mUnits[i]);
    }

    public void UpdateUI()
    {
        characterUI.nametTxt.text = SelectedUnit.name;
        characterUI.APTxt.text    = SelectedUnit.actionPoints + " / " + SelectedUnit.startAP;
        characterUI.HPTxt.text    = SelectedUnit.health + " / " + SelectedUnit.Starthealth;

        characterUI.npcHelpBt.SetActive(false);
        foreach (EnvironmentTile tile in GetActiveUnit().CurrentPosition.Connections)
        {
            if (tile.playerOcupied != null && tile.playerOcupied.GetComponent<NPC>() != null && tile.playerOcupied.GetComponent<NPC>().npcState == NPC.NPCState.Hiding)
            {
                characterUI.npcHelpBt.SetActive(true);
            }
        }
    }

    //  Display the enemy UI with percent chance of hitting - Called whenever the Render Texture is draged so updated only occur when needed
    public void UpdateAttackUI()
    {
        if (attackUI) GetComponent<AIManager>().ToggleAttackUI(SelectedUnit.GetAllVisible(), CritMultipler);
    }

    private bool CheckAllDone()
    {
        foreach (Character character in mUnits) if (character.actionPoints > 0) return false;

        return true;
    }

    public PlayerControledCharacter GetActiveUnit()
    {
        return SelectedUnit;
    }

    public void SetActiveCharacter(PlayerControledCharacter character)
    {
        // Reset tile materials
        ClearUI();

        // Set unit and UI
        SelectedUnit = character;
        UpdateUI();
        Camera.main.GetComponent<CameraMovement>().Target = SelectedUnit.transform.Find("Body1").transform;
        characterUI.playerCameraView.TargetChanged();

        characterUI.unitTypeIcon.sprite             = SelectedUnit.characterData.typeIcon;
        characterUI.unitAbilityIcon.sprite          = SelectedUnit.characterData.abilityIcon;
        characterUI.unitPositiveIcon.sprite         = SelectedUnit.characterData.positiveIcon;
        characterUI.unitNegativeIcon.sprite         = SelectedUnit.characterData.negativeIcon;
        characterUI.abilityIcon.sprite              = SelectedUnit.characterData.abilityIcon;
        characterUI.abilityNameTxt.text             = SelectedUnit.characterData.abilityName;
        characterUI.abilityAPCostTxt.text           = "AP : " + SelectedUnit.characterData.abilityAPCost.ToString(); ;
        characterUI.abilitydescriptionTxt.text      = SelectedUnit.characterData.abilityDescription;

        characterUI.unitAbilityTooltipImg.sprite    = SelectedUnit.characterData.abilityIcon;
        characterUI.unitPositiveTooltipImg.sprite   = SelectedUnit.characterData.positiveIcon;
        characterUI.unitNegativeTooltipImg.sprite   = SelectedUnit.characterData.negativeIcon;
        characterUI.unitNameAbilityTooltipTxt.text  = SelectedUnit.characterData.abilityName;
        characterUI.unitNamePositiveTooltipTxt.text = SelectedUnit.characterData.positiveName;
        characterUI.unitNameNegativeTooltipTxt.text = SelectedUnit.characterData.negativeName;

        characterUI.unitAbilityTooltipTxt.text      = SelectedUnit.characterData.abilityDescription;
        characterUI.unitPositiveTooltipTxt.text     = SelectedUnit.characterData.positiveDescription;
        characterUI.unitNegativeTooltipTxt.text     = SelectedUnit.characterData.negativeDescription;


    }

    // ||||| GAME ABILITIES |||||

    // If on an extractable tile remove the unit from the game
    public void Extract()
    {
        PlayerControledCharacter pcc = GetActiveUnit();

        if (pcc.CurrentPosition.CanExtract)
        {
            mExtractedUnits.Add(pcc);
            mUnits.Remove(pcc);
            pcc.CurrentPosition.playerOcupied = null;
            pcc.UIobj.SetActive(false);
            pcc.gameObject.SetActive(false);
            UpdateUI();
        }
    }

    // Increase defence of Unit
    public void HunkerDown()
    {
        SelectedUnit.HunkerDown();
        UpdateUI();
    }

    // If next to NPC and In Hidden state then activate
    public void RescutNPC()
    {
        foreach (EnvironmentTile tile in GetActiveUnit().CurrentPosition.Connections)
        {
            if (tile.playerOcupied != null && tile.playerOcupied.GetComponent<NPC>() != null && tile.playerOcupied.GetComponent<NPC>().npcState == NPC.NPCState.Hiding)
            {
                tile.playerOcupied.GetComponent<NPC>().Activate();
                UpdateUI();
            }
        }
    }

    public void BeginAttack()
    {
        attackUI = !attackUI;

        if  (attackUI)  { characterUI.renderScreen.GetComponent<CanvasGroup>().alpha = 1; GetComponent<AIManager>().ToggleAttackUI(SelectedUnit.GetAllVisible(), CritMultipler); }
        else            { characterUI.renderScreen.GetComponent<CanvasGroup>().alpha = 0; ClearUI(); Camera.main.GetComponent<CameraMovement>().CameraReset(); lookingAround = false; }

    }

    public void BeginAbility()
    {
        if (SelectedUnit.actionPoints >= SelectedUnit.characterData.abilityAPCost) SelectedUnit.Ability();
    }

    public void Attack(AIControlledCharacter enemy, float accuracy, bool critMulti = false)
    {
        characterUI.Hud.enabled = false;
        attackUI                = false;

        GetComponent<AIManager>().ClearAttackUI();

        // Show Attack UI 
        if (SelectedUnit.actionPoints > 0)
        {
            characterUI.activeShot.GetComponent<CanvasGroup>().alpha = 1;
            characterUI.shootUI.SetVisability(true);
            characterUI.shootUI.SetShooter(SelectedUnit);
            characterUI.shootUI.SetShootParameters(accuracy, critMulti);

            SelectedUnit.Attack(enemy);
        }

        UpdateUI();
        ClearUI();
    }

    // Reset UI after a shot
    public void ShotReset()
    {
        CritMultipler                                              = false;
        attackUI                                                   = false;
        characterUI.activeShot.GetComponent<CanvasGroup>().alpha   = 0;
        characterUI.renderScreen.GetComponent<CanvasGroup>().alpha = 0;
        characterUI.Hud.enabled                                    = true;

        GetComponent<AIManager>().ClearAttackUI();
    }

    // Display render to look around
    public void LookAround()
    {
        if (attackUI == false)
        {
            lookingAround = !lookingAround;

            if (lookingAround)
            {
                characterUI.renderScreen.GetComponent<CanvasGroup>().alpha = 1;
            }
            else
            {
                characterUI.renderScreen.GetComponent<CanvasGroup>().alpha = 0;
            }
        }
    }

    public void EndSelectedTurn()
    {
        SelectedUnit.actionPoints = 0;
        UpdateUI();
        ClearUI();
    }

    public void EndAllTurn()
    {
        foreach (Character character in mUnits) character.actionPoints = 0;
        UpdateUI();
        ClearUI();
    }

    public void ClearUI()
    {
        GetComponent<AIManager>().ClearAttackUI();
        if (SelectedUnit) mMap.ShowMoveMaterial(GetActiveUnit().CurrentPosition, GetActiveUnit().movementPerAP, true);
    }

    public void NextUnit()
    {
        ClearUI();
        ShotReset();
        

        unitIndex++;
        if (unitIndex > mUnits.Count - 1) unitIndex = 0;

        if  (mUnits[unitIndex].actionPoints > 0)   { FrameManager(); SetActiveCharacter(mUnits[unitIndex]); }
        else                                        NextUnit();
    }

    public void PreviousUnit()
    {
        ClearUI();
        ShotReset();

        unitIndex--;
        if (unitIndex < 0) unitIndex = mUnits.Count - 1;

        if  (mUnits[unitIndex].actionPoints > 0)   { FrameManager(); SetActiveCharacter(mUnits[unitIndex]); }
        else                                        PreviousUnit();
    }
}

// Used to clean up the inspector (Holds the UI data of the manager)
[System.Serializable]
public class CharacterUIHandler
{
    [Header("PLAYER UI")]
    public GameObject mUnitUI;
    public Transform content;

    [Header("HOLDERS")]
    public GameObject UI;

    [Header("PLAYER STATS")]
    public Text nametTxt;
    public Text HPTxt;
    public Text APTxt;

    [Header("UNIT ABILITY DETAILS")]
    public Image abilityIcon;
    public Text abilityNameTxt;
    public Text abilityAPCostTxt;
    public Text abilitydescriptionTxt;

    [Header("UNIT TYPE DETAILS")]
    public Image unitTypeIcon;
    public Image unitAbilityIcon;
    public Image unitPositiveIcon;
    public Image unitNegativeIcon;

    [Header("UNIT TYPE TOOLTIP")]
    public Image unitAbilityTooltipImg;
    public Image unitPositiveTooltipImg;
    public Image unitNegativeTooltipImg;
    public Text unitNameAbilityTooltipTxt;
    public Text unitNamePositiveTooltipTxt;
    public Text unitNameNegativeTooltipTxt;
    public Text unitAbilityTooltipTxt;
    public Text unitPositiveTooltipTxt;
    public Text unitNegativeTooltipTxt;

    [Header("SHOOTING")]
    public Transform renderScreen;
    public Transform activeShot;
    public Canvas Hud;
    public ActiveShot shootUI;
    public PlayerCameraView playerCameraView;

    [Header("NPC RESCUE")]
    public GameObject npcHelpBt;
}