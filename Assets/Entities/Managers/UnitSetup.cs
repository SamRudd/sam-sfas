﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitSetup : MonoBehaviour
{
    [Header("INPUT")]
    public InputField                       inputName;             // Unit name
    [SerializeField] private Dropdown       characterType;         // Unit character type

    [Header("Ability")]
    [SerializeField] private Image    abilityImg;
    [SerializeField] private Text     abilityNameTxt;
    [SerializeField] private Text     abilityDescriptionTxt;

    [Header("POSITIVE TRAIT")]
    [SerializeField] private Image    positiveImg;
    [SerializeField] private Text     positiveNameTxt;
    [SerializeField] private Text     positiveDescriptionTxt;

    [Header("NEGATIVE TRAIT")]
    [SerializeField] private Image    negativeImg;
    [SerializeField] private Text     negativeNameTxt;
    [SerializeField] private Text     negativeDescriptionTxt;

    [Header("CHARACTER DATA")]
    [SerializeField] private CharacterData[] unitTypes;           // Details of each type of player unit

    // Get the name and Unit type selected
    public KeyValuePair<string, string> GetSetupDetails()
    {
        return new KeyValuePair<string, string>(inputName.text, characterType.options[characterType.value].text);
    }

    private void Awake()
    {
        SetDetails();
    }

    // Display the Unit details to UI Elements
    public void SetDetails()
    {
        abilityImg.sprite           = unitTypes[characterType.value].abilityIcon;
        abilityNameTxt.text         = unitTypes[characterType.value].abilityName;
        abilityDescriptionTxt.text  = unitTypes[characterType.value].abilityDescription;

        positiveImg.sprite          = unitTypes[characterType.value].positiveIcon;
        positiveNameTxt.text        = unitTypes[characterType.value].positiveName;
        positiveDescriptionTxt.text = unitTypes[characterType.value].positiveDescription;

        negativeImg.sprite          = unitTypes[characterType.value].negativeIcon;
        negativeNameTxt.text        = unitTypes[characterType.value].negativeName;
        negativeDescriptionTxt.text = unitTypes[characterType.value].negativeDescription;
    }
}
