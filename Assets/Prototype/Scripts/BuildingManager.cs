﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    public TileManager tm;
    public List<Building> buildings;

    private void Update()
    {
        foreach (Building building in buildings)
        {
            building.BuildingUpdate();
        }
    }

    public void AddBuilding(GameObject obj, Tile tile)
    {
        // Check if building can be added
        // instanitate building
        // add building 
        Vector3 tileLC = tile.transform.position - new Vector3(0.3f, 0.0f, 0.3f);
        GameObject newObj = Instantiate(obj, tileLC, Quaternion.identity);

        Vector3 size = newObj.GetComponent<Collider>().bounds.size;

        float w = (size.x / 2);// * (newObj.GetComponent<Building>().sizeX - 1);
        float l = (size.z / 2);// * (newObj.GetComponent<Building>().SizeZ - 1);

        //w -= 1;
        //l -= 1;

        if (newObj.GetComponent<Building>().sizeX == 1) w = 0.3f;
        if (newObj.GetComponent<Building>().SizeZ == 1) l = 0.3f;

        Vector3 offset = new Vector3(w, 0.0f, l);
        Vector3 newPos = newObj.transform.position + offset;

        Debug.Log("Position: " + newObj.transform.position + " | Offset: " + offset + " | New Position: " + newPos);

        newObj.transform.position = newPos;

        buildings.Add(newObj.GetComponent<Building>());
        tm.BuildingPlaced(newObj.GetComponent<Building>());
        newObj.GetComponent<Building>().BuildingInit();
    }

}
