﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TurnManager : MonoBehaviour
{
    public enum TurnState { WaitingForTurn, Active, TurnEnded,  }

    [Header("STATE")]
    public TurnState turnState;                 // Current state of the manager

    public abstract void Init();                // Called once at the begining of the game.
    public abstract void StartTurn();           // Called once at the begining of each turn.
    protected abstract void EndTurn();          // Called once at the end of each turn.
    public abstract void UpdateManager();       // Called every frame while it's this managers turn.
    public abstract void FrameManager();        // Called every frame
    public abstract bool IsCompleted();         // Is the manager done (has no units to control)
}
