﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIMouseInteraction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    // Increase size and change colour of UI elements specified when the mouse if hovering over

    [Header("COLOURS")]
    [SerializeField] private Color resetColour = Color.white;
    [SerializeField] private Color highlightColour;

    [Header("SCALE")]
    [SerializeField] private Vector3 baseScale = Vector3.one;
    [Range(1.0f, 2.0f)]
    [SerializeField] private float scaleFactor;

    [Header("EFFECTED")]
    [SerializeField] private Image[] elementsAffected;            // Elements that will be scaled and colour changed

    public void OnPointerEnter(PointerEventData eventData)
    {
        // Scale and change colour
        GetComponent<RectTransform>().localScale = Vector3.one * scaleFactor;
        foreach (Image img in elementsAffected) img.color = highlightColour;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // Scale and change colour (Reset)
        GetComponent<RectTransform>().localScale = baseScale;
        foreach (Image img in elementsAffected) img.color = resetColour;
    }
}
