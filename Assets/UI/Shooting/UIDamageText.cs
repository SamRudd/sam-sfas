﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDamageText : MonoBehaviour
{
    [SerializeField] private float destroyTime  = 3f;               // The lifetime of the object after which it will be destroyed;
    [SerializeField] private Vector3 offset     = Vector3.up * 7;   // Where the text will appear in relation to the unit

    public void Init(string text, Color textColour)
    {
        // Set position
        offset += new Vector3(Random.Range(-1, 1), 0.0f, 0.0f);
        transform.position += offset;

        // Destroy after time
        Destroy(gameObject, destroyTime);

        // Set colours
        GetComponent<TextMesh>().text = text;
        GetComponent<TextMesh>().color = textColour;
    }

    private void Update()
    {
        // Face camera and move up
        transform.LookAt(Camera.main.transform.position);
        transform.position += Vector3.up * Time.deltaTime;
    }
}
