﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class AIManager : TurnManager
{
    [Header("UI ELEMENTS")]
    [SerializeField] private GameObject   enemieUIElement;        // UI for the zombies that can be shot
    [SerializeField] private Transform    listContents;           // The list which hold the UI elements

    public int          DeathCount  { get; set; } = 0;          // Tracks how many zombies have been killed
    public int          PackIndex   { get; private set; } = 0;  // Tracks which pack is being controlled
    public List<Pack>   Packs       { get; private set; }       // Control groups of AI controlled units

    private List<EnemieButton> enemieUI;    // Zombie target UIs 
    private CameraMovement cameraMovement;

    public override void Init()
    {
        cameraMovement = Camera.main.GetComponent<CameraMovement>();

        Pack[] tempPacks = FindObjectsOfTypeAll(typeof(Pack)) as Pack[];
        Packs            = new List<Pack>(tempPacks);
        enemieUI         = new List<EnemieButton>();

        foreach (Pack p in Packs) p.Init();
    }

    // Resets all the packs then starts the turn
    public override void StartTurn()
    {
        // Remove packs where all the units are dead
        for (int i = 0; i < Packs.Count; i++)
        {
            if (Packs[i].Units.Count == 0)
            {
                Destroy(Packs[i].gameObject);
                Packs.RemoveAt(i);
            }
        }

        // Reset packs
        foreach (Pack p in Packs) p.PackReset();
        PackIndex = 0;

        // Reset camera and UI
        cameraMovement.CameraReset();
        ClearAttackUI();

        // Start the turn
        turnState = TurnState.Active;
        TakeEnemyTurn();
    }

    public override void UpdateManager()
    {
        // End game if no enemies remain
        if (Packs.Count == 0)
        {
            EndTurn();
            GetComponent<Game>().EndGame();
            return;
        }

        // If all units have taken turn then end the managers turn
        if (CheckAllDone())
        {
            EndTurn();
            return;
        }

        // If the current unit has finished their turn start the end turn
        if (Packs[PackIndex].turnState == Pack.States.TurnTaken && (PackIndex + 1) < Packs.Count)
        {
            PackIndex++;
            TakeEnemyTurn();
        }
    }

    public override void FrameManager()
    {
        // Check if any of the units can see any player or NPC units
        foreach (Pack pack in Packs) pack.VisionTest();
    }

    protected override void EndTurn()
    {
        turnState = TurnState.TurnEnded;
    }

    private bool CheckAllDone()
    {
        foreach (Pack p in Packs) if (p.turnState != Pack.States.TurnTaken) return false;

        return true;
    }

    public void TakeEnemyTurn()
    {
        if (Packs.Count > 0) StartCoroutine(Packs[PackIndex].EnemyTurn());
    }

    // Return all units from every pack
    public List<AIControlledCharacter> GetAllUnits()
    {
        List<AIControlledCharacter> allUnits = new List<AIControlledCharacter>();

        foreach (Pack p in Packs) foreach (AIControlledCharacter unit in p.Units) allUnits.Add(unit);

        return allUnits;
    }

    // Display UI elements that can be used to target zombies along with the percent chance of hitting them
    public void ToggleAttackUI(PlayerControledCharacter.EnemyVisibility[] enemies, bool critMultipler = false)
    {
        GetComponent<CharacterManager>().ClearUI();

        // Create UI elements for each of the zombies visible by the players unit
        foreach (PlayerControledCharacter.EnemyVisibility character in enemies)
        {
            EnemieButton uiElement = Instantiate(enemieUIElement).GetComponent<EnemieButton>();
            uiElement.character    = character.character;
            uiElement.cm           = GetComponent<CharacterManager>();
            uiElement.Accuracy     = character.accuracy;

            uiElement.SetText(uiElement.Accuracy.ToString() + "%", critMultipler);
            enemieUI.Add(uiElement.GetComponent<EnemieButton>());
        }

        // Sort the list by the accuarcy to hit the Target
        enemieUI.Sort(delegate (EnemieButton a, EnemieButton b) { return b.Accuracy.CompareTo(a.Accuracy); });

        // Add the sorted list in oder to the scroll view
        for (int j = 0; j < enemieUI.Count; j++) enemieUI[j].transform.parent = listContents;
    }

    // Destroy all Attack UI elements of the list
    public void ClearAttackUI()
    {
        enemieUI.Clear();
        enemieUI              = new List<EnemieButton>();
        EnemieButton[] enButs = listContents.GetComponentsInChildren<EnemieButton>();

        for (int j = 0; j < enButs.Length; j++) Destroy(enButs[j].gameObject);
    }

    // Used by ToggleAttackUI when sorting the enemies by best chance to hit
    static float SortByScore(EnemieButton p1, EnemieButton p2)
    {
        return p1.Accuracy.CompareTo(p2.Accuracy);
    }

    // If all the units been killed when return is returned 
    public override bool IsCompleted()
    {
        // Remove packs that have had all their unit killed
        for (int i = 0; i < Packs.Count; i++)
        {
            if (Packs[i].Units.Count == 0)
            {
                Destroy(Packs[i].gameObject);
                Packs.RemoveAt(i);
            }
        }

        if (Packs.Count == 0) return true;

        return false;
    }
}
