﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetupStats : MonoBehaviour
{
    [SerializeField] private Text missionResultTxt;

    [SerializeField] private Text NPCSavedTxt;
    [SerializeField] private Text NPCLostTxt;

    [SerializeField] private Text unitsDeathsTxt;
    [SerializeField] private Text unitsWoundedTxt;

    [SerializeField] private Text zombiesKilledTxt;
    [SerializeField] private Text zombiesAmountTxt;

    public void SetStats(CharacterManager cMan, NPCManager npcMan, AIManager aiMan)
    {
        // Mission State
        if (!cMan.AllUnitsDead()) missionResultTxt.text = "Mission SUCCESS";
        else missionResultTxt.text = "Mission FAILURE";

        // NPCs
        NPCSavedTxt.text = npcMan.ExtractedCount.ToString();
        NPCLostTxt.text = npcMan.DeathCount.ToString();

        // Zombies
        zombiesKilledTxt.text = aiMan.DeathCount.ToString();

        // Units
        unitsDeathsTxt.text     = cMan.GetDeadUnits().ToString();
        unitsWoundedTxt.text    = cMan.GetWoundedUnits().ToString();
    }

}
