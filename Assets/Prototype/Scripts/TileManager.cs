﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public GameObject tempTile;

    public int width, height;
    GameObject[,] tiles;

    public Tile currentTile;

    // Start is called before the first frame update
    void Start()
    {
        tiles = new GameObject[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                tiles[x, y] = Instantiate(tempTile, new Vector3(x * 1.0f, 0.0f, y * 1.0f), Quaternion.identity, this.transform);
                tiles[x, y].GetComponent<Tile>().tm = this;
                tiles[x, y].GetComponent<Tile>().x = x;
                tiles[x, y].GetComponent<Tile>().y = y;
                tiles[x, y].transform.name = x + " " + y;
            }
        }

    }

    public Tile GetTile(int x, int y)
    {
        Debug.Log("X: " + x + " Width: " + width);
        Debug.Log("Y: " + y + " Height: " + height);

        if (x >= 0 && x < width && y >= 0 && y < height)
        {
            Debug.Log("Return");
            return tiles[x, y].GetComponent<Tile>();
        }

        Debug.Log("Null");
        return null;
    }

    // Add a tile clicked on notify for all controls, pass tile
    // Add a tile hover on notify for all controls, pass tile

    public void BuildingPlaced(Building building)
    {
        for (int x = 0; x < building.sizeX; x++)
        {
            for (int y = 0; y < building.SizeZ; y++)
            {
                tiles[x + currentTile.x, y + currentTile.y].GetComponent<Tile>().placed = building;
            }
        }
    }

    public bool CanPlace(int rootX, int rootY, int sizeX, int sizeY)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (tiles[x + rootX, y + rootY].GetComponent<Tile>().placed != null)
                {
                    Debug.Log("Can't place");
                    return false;
                }
            }
        }

        Debug.Log("Can place");
        return true;
    }

    public bool CanPlace(int sizeX, int sizeY)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (tiles[x + currentTile.x, y + currentTile.y].GetComponent<Tile>().placed != null)
                {
                    Debug.Log("Can't place");
                    return false;
                }
            }
        }

        Debug.Log("Can place");
        return true;
    }
}
