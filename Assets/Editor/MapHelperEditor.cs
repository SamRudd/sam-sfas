﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Environment))]
public class MapHelperEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Environment map = target as Environment;
        if (DrawDefaultInspector())
        {

        }

        if (GUILayout.Button("Generate Map"))
        {
            map.NameMap();
        }
    }
}
