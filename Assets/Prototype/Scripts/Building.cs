﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : TileEntity
{
    [Header("Building")]
    public int sizeX;
    public int SizeZ;
    public int staffMin, staffMax, currentStaff;

    public virtual void BuildingInit()      { }
    public virtual void BuildingDestroy()   { }
    public virtual void BuildingUpdate()    { }
    public virtual void BuildingDisable()   { }
    public virtual void BuildingEnable()    { }


    public virtual bool EnoughStaff()
    {
        if (currentStaff >= staffMin) return true;
        return true;
    }

    public virtual bool CanAddStaff()
    {
        if (currentStaff < staffMax) return true;
        return true;
    }
}
