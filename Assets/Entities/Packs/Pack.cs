﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pack : MonoBehaviour
{
    public enum PackVisabilityStatus { Hidden, Visable,}
    public enum PackStatus { Active, InActive, }
    public enum States { WaitingForTurn, TurnTaken, TurnInProgress, }

    [Header("STATES")]
    public PackVisabilityStatus visability;
    public PackStatus           status;
    public States               turnState;

    [Header("MANAGERS")]
    [SerializeField] private Environment map;
    [SerializeField] private AIManager ai;

    [Header("AGENTS")]
    [SerializeField] private AIControlledCharacter AIPrefab;
    [SerializeField] private int numOfAgents;

    [Header("UI")]
    [SerializeField] private GameObject unitUI;
    [SerializeField] private Transform content;
    [SerializeField] private Announcements announcements;

    public List<Character> SightedNPCs { get; private set; }
    public List<AIControlledCharacter> Units { get; private set; }

    private Camera packCamera;

    public void Init()
    {
        SightedNPCs = new List<Character>();
        Units       = new List<AIControlledCharacter>();
        packCamera  = GetComponentInChildren<Camera>();

        // Turn of UI
        foreach (MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>()) renderer.enabled = false;

        // Get world position
        int startX = (int)(transform.position.x / map.TileSize);
        int startY = (int)(transform.position.z / map.TileSize);

        // Add units
        for (int i = 0; i < numOfAgents; i++) AddUnit(startX + i, startY);
    }

    public void AddUnit(int x, int y)
    {
        // Get a random rotation of a multiple of 90
        float[] rotations = new float[4] { 0.0f, 90.0f, 180.0f, 360.0f };
        float yRotation = rotations[Random.Range(0, rotations.Length)];

        AIControlledCharacter unit;
        GameObject ui;

        // Create a zombie and its UI
        unit    = Instantiate(AIPrefab, transform);
        ui      = Instantiate(unitUI, content);

        unit.transform.position            = map.GetTile(x, y).Position;
        unit.transform.rotation            = Quaternion.Euler(0.0f, yRotation, 0.0f);
        unit.CurrentPosition               = map.GetTile(x, y);
        unit.CurrentPosition.playerOcupied = unit;
        unit.transform.name                = "Enemy " + Units.Count;
        unit.Manager                       = ai;
        unit.Map                          = map;
        unit.UIobj                         = ui;
        unit.AIPack                          = this;

        // Make it invisible
        if (visability == PackVisabilityStatus.Hidden) foreach (SkinnedMeshRenderer skin in unit.GetComponentsInChildren<SkinnedMeshRenderer>()){ skin.enabled = false; }

        ui.GetComponent<UnitUI>().Unit = unit;
        Units.Add(unit);
    }

    // Reset the pack state and all of its units
    public void PackReset()
    {
        turnState = States.WaitingForTurn;
        foreach (Character character in Units) character.ResetCharacter();
    }

    public void VisionTest()
    {
        // NPC ARE ONLY ADDED AT THE MOMENT WHEN THEY ARE INACTIVE FIX
        foreach (AIControlledCharacter unit in Units)
        {
            // Set Pack camera to view from perspective of unit
            packCamera.transform.position = unit.cameraHolder.position;
            packCamera.transform.rotation = unit.cameraHolder.rotation;
            packCamera.transform.parent   = unit.cameraHolder;

            if (unit.VisionTest())
            {
                if (visability == PackVisabilityStatus.Hidden) StartCoroutine(SetVisible(unit));

                StartCoroutine(AnnouncePack(unit));
            }
        }     
    }

    public IEnumerator EnemyTurn(bool reset = true)
    {
        turnState      = States.TurnInProgress;
        int enemyIndex = 0;

        foreach (AIControlledCharacter enemy in Units)
        {
            if (reset) enemy.ResetCharacter();
            enemy.state = Character.States.WaitingForTurn;
        }

        while (enemyIndex < Units.Count)
        {
            if (Units[enemyIndex].state == Character.States.WaitingForTurn)
            {
                Camera.main.GetComponent<CameraMovement>().Target = Units[enemyIndex].transform;
                StartCoroutine(Units[enemyIndex].TakeEnemyTurn());
            }

            if (Units[enemyIndex].state == Character.States.TurnTaken) enemyIndex++;

            yield return 0;
        }

        Debug.Log("Done");
        turnState = States.TurnTaken;
    }

    public void KillUnit(AIControlledCharacter unit)
    {
        unit.CurrentPosition.playerOcupied = null;

        Units.Remove(unit);
        unit.UIobj.SetActive(false);
        unit.gameObject.SetActive(false);

        ai.DeathCount++;
    }

    public IEnumerator SetVisible(Character spotter)
    {
        Transform trans = null;

        foreach (Character ai in Units)
        {
            foreach (SkinnedMeshRenderer skin in ai.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                skin.enabled = true;
            }

            ai.UIobj.GetComponent<CanvasGroup>().alpha = 1;
        }


        visability = Pack.PackVisabilityStatus.Visable;

        if(Camera.main.GetComponent<CameraMovement>().Target.GetComponent<Scout>() ||
            Camera.main.GetComponent<CameraMovement>().Target.GetComponent<Soldier>()) trans = Camera.main.GetComponent<CameraMovement>().Target;

        Camera.main.GetComponent<CameraMovement>().Target = spotter.transform;

        announcements.SpottedPack();
        yield return new WaitForSeconds(3);

        if (trans != null) Camera.main.GetComponent<CameraMovement>().Target = trans;
    }

    public IEnumerator AnnouncePack(AIControlledCharacter spotter)
    {
        Transform trans = null;

        Debug.Log("PACK ACTIVE");
        status = PackStatus.Active;

        if (Camera.main.GetComponent<CameraMovement>().Target.GetComponent<Scout>() ||
            Camera.main.GetComponent<CameraMovement>().Target.GetComponent<Soldier>()) trans = Camera.main.GetComponent<CameraMovement>().Target;

        Camera.main.GetComponent<CameraMovement>().Target = spotter.transform;

        announcements.PackActive();
        yield return new WaitForSeconds(3);

        if (trans != null) Camera.main.GetComponent<CameraMovement>().Target = trans;

        foreach (AIControlledCharacter ai in Units) ai.actionPoints = 1;
        StartCoroutine(EnemyTurn(false));

    }
}
