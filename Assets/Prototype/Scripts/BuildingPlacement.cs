﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPlacement : Controls
{
    public GameObject obj;
    public GameObject buildingTemp;

    public TileManager tm;
    public BuildingManager bm;

    public GameObject wall;
    public GameObject wallTemp;

    public enum BuildingMode { Buildings, Walls, }
    public BuildingMode mode;

    public Material placementMat;
    public Material builtMat;

    // Get Scene Manager

    public override void ControlInit()
    {
        // Create new Object
        // Apply see through material
    }

    public override void ControlUpdate()
    {
        if (mode == BuildingMode.Buildings)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                buildingTemp = Instantiate(obj);
                buildingTemp.GetComponent<Renderer>().material = placementMat;
            }



            if (Input.GetMouseButtonDown(0))
            {
                if (tm.CanPlace(obj.GetComponent<Building>().sizeX, obj.GetComponent<Building>().SizeZ)) bm.AddBuilding(obj, tm.currentTile);
            }
        }
        else if (mode == BuildingMode.Walls)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                wallTemp = Instantiate(wall);
                wallTemp.GetComponent<Renderer>().material = placementMat;
            }
            RaycastHit hit;
            Ray ray;
            Vector2 mouse   = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            ray             = Camera.main.ScreenPointToRay(mouse);

            if (Physics.Raycast(ray, out hit, 10))
            {
                if (hit.point.x < tm.currentTile.transform.position.x - 0.3f)
                {
                    if (wallTemp != null)
                    {
                        wallTemp.transform.position = new Vector3(tm.currentTile.transform.position.x - 0.5f, tm.currentTile.transform.position.y + 0.15f, tm.currentTile.transform.position.z);
                        wallTemp.transform.eulerAngles = new Vector3(0, 90, 0);

                        if (Input.GetMouseButtonDown(0))
                        {
                            GameObject w = Instantiate(wallTemp);
                            Tile boarderTile = tm.GetTile(tm.currentTile.x - 1, tm.currentTile.y);
                            tm.currentTile.surround[(int)Tile.Transistions.LeftToRight] = w.GetComponent<Wall>();
                            w.GetComponent<Renderer>().material = builtMat;

                            if (boarderTile != null) boarderTile.surround[(int)Tile.Transistions.RightToLeft] = w.GetComponent<Wall>();
                        }
                    }
                    Debug.Log("Left");
                }

                if (hit.point.x > tm.currentTile.transform.position.x + 0.3f)
                {
                    if (wallTemp != null)
                    {
                        wallTemp.transform.position = new Vector3(tm.currentTile.transform.position.x + 0.5f, tm.currentTile.transform.position.y + 0.15f, tm.currentTile.transform.position.z);
                        wallTemp.transform.eulerAngles = new Vector3(0, 90, 0);

                        if (Input.GetMouseButtonDown(0))
                        {
                            GameObject w = Instantiate(wallTemp);
                            Tile boarderTile = tm.GetTile(tm.currentTile.x + 1, tm.currentTile.y);
                            tm.currentTile.surround[(int)Tile.Transistions.RightToLeft] = w.GetComponent<Wall>();
                            w.GetComponent<Renderer>().material = builtMat;

                            if (boarderTile != null) boarderTile.surround[(int)Tile.Transistions.LeftToRight] = w.GetComponent<Wall>();
                        }
                    }
                    Debug.Log("Right");
                }

                if (hit.point.z < tm.currentTile.transform.position.z - 0.3f)
                {
                    if (wallTemp != null)
                    {
                        wallTemp.transform.position = new Vector3(tm.currentTile.transform.position.x, tm.currentTile.transform.position.y + 0.15f, tm.currentTile.transform.position.z - 0.5f);
                        wallTemp.transform.eulerAngles = new Vector3(0, 0, 0);

                        if (Input.GetMouseButtonDown(0))
                        {
                            GameObject w = Instantiate(wallTemp);
                            Tile boarderTile = tm.GetTile(tm.currentTile.x, tm.currentTile.y - 1);
                            tm.currentTile.surround[(int)Tile.Transistions.BottomToTop] = w.GetComponent<Wall>();
                            w.GetComponent<Renderer>().material = builtMat;

                            if (boarderTile != null) boarderTile.surround[(int)Tile.Transistions.TopToBottom] = w.GetComponent<Wall>();
                        }
                    }
                    Debug.Log("Down");
                }

                if (hit.point.z > tm.currentTile.transform.position.z + 0.3f)
                {
                    if (wallTemp != null)
                    {
                        wallTemp.transform.position = new Vector3(tm.currentTile.transform.position.x, tm.currentTile.transform.position.y + 0.15f, tm.currentTile.transform.position.z + 0.5f);
                        wallTemp.transform.eulerAngles = new Vector3(0, 0, 0);

                        if (Input.GetMouseButtonDown(0))
                        {
                            GameObject w = Instantiate(wallTemp);
                            Tile boarderTile = tm.GetTile(tm.currentTile.x, tm.currentTile.y + 1);
                            tm.currentTile.surround[(int)Tile.Transistions.TopToBottom] = w.GetComponent<Wall>();
                            w.GetComponent<Renderer>().material = builtMat;

                            if (boarderTile != null) boarderTile.surround[(int)Tile.Transistions.BottomToTop] = w.GetComponent<Wall>();
                        }
                    }
                    Debug.Log("Up");
                }
            }
        }
    }

}
