﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EnemieButton : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler, IPointerDownHandler
{
    public AIControlledCharacter character { get; set; }

    [SerializeField] private Text percentText;
    public CharacterManager cm;

    public float Accuracy { get; set; }
    private bool critMultipler = false;

    void Start()
    {
        percentText = GetComponentInChildren<Text>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Camera.main.GetComponent<CameraMovement>().BeginCharacterLookAt(character.transform);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Camera.main.GetComponent<CameraMovement>().StopCharacterLookAt();
    }

    public void SetText(string line, bool critMulti)
    {
        critMultipler = critMulti;

        percentText = GetComponentInChildren<Text>();
        percentText.text = line;

        if (critMultipler)
        {
            GetComponent<Image>().color = Color.yellow;
            percentText.color = Color.yellow;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        cm.Attack(character, Accuracy, critMultipler);
    }
}
