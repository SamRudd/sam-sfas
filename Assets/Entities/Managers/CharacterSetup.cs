﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSetup : MonoBehaviour
{
    public List<KeyValuePair<string, string>> setup;
    private List<int> randFirstPicks;
    private List<int> randLastPicks;

    [Header("FIRST NAMES")]
    [SerializeField] private string[] randomFirstNames;     // List of first names

    [Header("LAST NAMES")]
    [SerializeField] private string[] randomLastNames;      // List of last names

    private UnitSetup[] units;  // UI Element for each unit in character selection

    private void Awake()
    {
        setup           = new List<KeyValuePair<string, string>>();
        randFirstPicks  = new List<int>();
        randLastPicks   = new List<int>();

        units = GameObject.FindObjectsOfType<UnitSetup>();

        // Generate a random name for each unit to be put in the Input text field
        foreach (UnitSetup unit in units) unit.inputName.text = GetRandomName();
    }

    // Get a random first and last name
    public string GetRandomName()
    {
        int randFirstIndex  = Random.Range(0, randomFirstNames.Length - 1);
        int randLastIndex   = Random.Range(0, randomLastNames.Length - 1);

        if (randFirstPicks.Contains(randFirstIndex) || randLastPicks.Contains(randLastIndex))
        {
            return GetRandomName();
        }
        else
        {
            randFirstPicks.Add(randFirstIndex);
            randLastPicks.Add(randLastIndex);

            return randomFirstNames[randFirstIndex] + " " + randomLastNames[randLastIndex];
        }

    }

    // Called when creating player units
    public List<KeyValuePair<string, string>> GetCharacters()
    {
        // return name and unit type
        foreach (UnitSetup unit in units) setup.Add(unit.GetSetupDetails());

        return setup;
    }

}
