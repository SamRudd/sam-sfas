﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioSource gameBackground;
    [SerializeField] private AudioClip button;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private float soundCutoff = -40.0f;

    private AudioSource source;

    public void SetVolume(float volume)
    {
        if (volume == -soundCutoff) volume = -80.0f;

        audioMixer.SetFloat("MasterVolume", volume);
    }

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayButton()
    {
        source.clip = button;
        source.Play();
    }

    public void PlayBackgroud()
    {
        StartCoroutine(PlayAudioClip(gameBackground, 15.0f));
    }

    IEnumerator PlayAudioClip(AudioSource audioSource, float time)
    {
        //audioSource.clip = appreciationClip;
        audioSource.Play();
        while (audioSource.isPlaying)
        {
            // do nothing and keep returning while audio is still playing
            yield return null;
        }

        // wait a bit before playing next clip
        yield return new WaitForSeconds(time);

        //audioSource.clip = afterAppreciationClip;
        audioSource.Play();
    }
}
