﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableSelf : MonoBehaviour
{
    // Disable gameobject when called
    public void Disable()
    {
        this.gameObject.SetActive(false);
    }

}
