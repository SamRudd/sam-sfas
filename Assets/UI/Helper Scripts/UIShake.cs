﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShake : MonoBehaviour
{
    // Rotate between min and max on the Z axis for UI Elements

    [Header("ROTATION STATS")]
    [SerializeField] private float minRotation = 0.0f;      
    [SerializeField] private float maxRotation = 10.0f;
    [SerializeField] private float rotationSpeed = 5.0f;

    private RectTransform   rect;

    private void Awake()
    {
        rect     = GetComponent<RectTransform>();
    }

    private void Update()
    {
        // Bounce back and forth between the min and max rotation
        float time       = Mathf.PingPong(Time.time * rotationSpeed, 1);
        rect.eulerAngles = Vector3.Lerp(new Vector3(0.0f, 0.0f, minRotation), new Vector3(0.0f, 0.0f, maxRotation), time);
    }


}
