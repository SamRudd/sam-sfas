﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ActiveShot : MonoBehaviour
{
    enum State { Waiting, Sliding, Post }
    State FireState = State.Waiting;

    [Header("MANAGER")]
    [SerializeField] private CharacterManager cm;

    [Header("UI")]
    [SerializeField] private Image      slider;
    [SerializeField] private GameObject shootBar;
    [SerializeField] private Image      standard;
    [SerializeField] private Image      perfect;

    [Header("MOTION")]
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private Vector2 activeTemplateRange    = new Vector2(116, 155);    // The range in which an active shot occures
    [SerializeField] private Vector2 perfectTemplateRange   = new Vector2(100, 115);    // The range in which an perfect shot occures (Crit)
    [SerializeField] private float critMultiplerFactor      = 20.0f;                    // How much to increase the crit zone by

    [Header("Controls")]
    [SerializeField] private KeyCode trigger = KeyCode.Space;   // Used to start and stop the slider

    private Vector2 activeRange;    // The values used to calculate each time for the active range
    private Vector2 perfectRange;   // The values used to calculate each time for the perfect range

    private const float accuracyDifficulity = 5.0f;     // The Accuracy will be devided by this to get a more accurate range
    private const float critAccuracyCutOff  = 15.0f;    // The value at which if Accuracy is below no crital chance is presented
    private const float barHeight           = 10.0f;    // The height of the active shot bar
    private const float barWidth            = 320.0f;   // The width of the active shot bar

    private PlayerControledCharacter unit;              // The unit doinging the attacking

    // Coroutines
    private Coroutine Shoot;

    private void Awake()
    {
        // Set to Default
        standard.rectTransform.anchoredPosition = new Vector3(activeTemplateRange.x, 0.0f, standard.rectTransform.position.z);
        standard.rectTransform.sizeDelta        = new Vector2(activeTemplateRange.y - activeTemplateRange.x, barHeight);

        perfect.rectTransform.anchoredPosition  = new Vector3(perfectTemplateRange.x, 0.0f, perfect.rectTransform.position.z);
        perfect.rectTransform.sizeDelta         = new Vector2(perfectTemplateRange.y - perfectTemplateRange.x, barHeight);
    }

    public void SetVisability(bool state)
    {
        shootBar.SetActive(state);
    }

    public void SetShooter(PlayerControledCharacter player)
    {
        unit = player;
    }

    public void SetShootParameters(float accuracy, bool critMulti = false)
    {
        bool showCrit = false;

        // Only show crit if accuracy is high enough
        if (accuracy > critAccuracyCutOff) showCrit = true;       

        // Scale down range to make it more challenging
        accuracy /= accuracyDifficulity;

        // Calculate size
        int standardSize  = Mathf.RoundToInt(3.2f * accuracy);
        int standardStart = Mathf.RoundToInt(activeTemplateRange.y) - standardSize;

        // Set Standard size
        activeRange.x = standardStart;
        activeRange.y = standardStart + standardSize;

        // Calculate critial chance
        if (showCrit)
        {
            // Set standard perfect range
            perfectRange.x = perfectTemplateRange.x;
            perfectRange.y = perfectTemplateRange.y;

            // If ability is active increase the critical chance area
            if (critMulti)
            {
                activeRange.x   -= critMultiplerFactor;
                activeRange.y   -= critMultiplerFactor;
                perfectRange.x  -= critMultiplerFactor;
            }
        }
        else
        {
            // Make the crit chance impossible if accuracy is too low
            perfectRange.x = -1.0f;
            perfectRange.y = -1.0f;
        }

        // Adjust the UI to fit the new parameters
        standard.rectTransform.anchoredPosition = new Vector3(activeRange.x, 0.0f, standard.rectTransform.position.z);
        standard.rectTransform.sizeDelta        = new Vector2(activeRange.y - activeRange.x, barHeight);

        perfect.rectTransform.anchoredPosition  = new Vector3(perfectRange.x, 0.0f, perfect.rectTransform.position.z);
        perfect.rectTransform.sizeDelta         = new Vector2(perfectRange.y - perfectRange.x, barHeight);
    }

    private void Update()
    {
        if      (FireState == State.Waiting && Input.GetKeyDown(trigger)) BeginShot();
        else if (FireState == State.Sliding && Input.GetKeyDown(trigger)) Trigger();

        if (FireState == State.Sliding && slider.rectTransform.anchoredPosition.x >= barWidth) FailedReload(slider.rectTransform.anchoredPosition.x);
    }

    public void BeginShot()
    {
        FireState    = State.Sliding;
        slider.color = Color.white;
        shootBar.SetActive(true);
        Shoot       = StartCoroutine(Sliding());
    }

    public void Trigger()
    {
        float value = slider.rectTransform.anchoredPosition.x;

        // Check which range the slider once stopped in and call function
        if      (value >= perfectRange.x && value <= perfectRange.y)    PerfectShot(value);
        else if (value >= activeRange.x && value <= activeRange.y)      ActiveReload(value);
        else                                                            FailedReload(value);

        if (Shoot != null) StopCoroutine(Shoot);          
    }

    public float speed = 3.0f;

    private IEnumerator Sliding()
    {
        yield return new WaitForEndOfFrame();

        // Move the slider 
        for (float t = 0; t < 1; t += Time.deltaTime / speed)
        {
            float value                           = Mathf.Lerp(0, barWidth, curve.Evaluate(t));
            slider.rectTransform.anchoredPosition = new Vector2(value, 0);

            yield return null;
        }

        StartCoroutine(Finish(0.0f, false));
    }

    private IEnumerator Finish(float duration, bool perfect)
    {
        // Turn off the shot bar after delay

        yield return new WaitForSeconds(1.0f);
        slider.rectTransform.anchoredPosition = new Vector2(0, 0);

        FireState = State.Waiting;
        shootBar.SetActive(false);
    }

    private void PerfectShot(float value)
    {
        FireState       = State.Waiting;
        slider.color    = Color.green;

        cm.ShotReset();
        unit.UIShoot(true, true);
        GetComponent<AudioSource>().Play();
        StartCoroutine(Finish(0.1f, true));
    }

    private void ActiveReload(float value)
    {
        FireState = State.Waiting;

        cm.ShotReset();
        unit.UIShoot(true, false);
        GetComponent<AudioSource>().Play();
        StartCoroutine(Finish(0.1f, false));
    }

    private void FailedReload(float value)
    {
        FireState       = State.Waiting;
        slider.color    = Color.red;

        cm.ShotReset();
        unit.UIShoot(false, false);
        GetComponent<AudioSource>().Play();
        StartCoroutine(Finish(0.1f, false));
    }
}
