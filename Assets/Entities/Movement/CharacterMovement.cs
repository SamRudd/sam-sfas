﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : Controls
{
    [Header("STATE")]
    [SerializeField] private bool enableMovement = false;   // Enabled when the moveBt is clicked
    
    [Header("INTERACTION")]
    [SerializeField] private LayerMask tileMask;

    private Environment map;

    public override void ControlInit()
    {
        map = GetComponentInChildren<Environment>();
    }

    public override void ControlUpdate()
    {
        if (Input.GetMouseButtonDown(0) && enableMovement)
        {
            // Check to see if the player has clicked a tile and if they have, try to find a path to that 
            // tile. If we find a path then the character will move along it to the clicked tile. 

            Ray screenClick = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hs = Physics.RaycastAll(screenClick);

            if (hs.Length > 0)
            {
                EnvironmentTile tile = null;

                foreach (RaycastHit h in hs)
                {
                    if (h.transform.GetComponent<EnvironmentTile>() != null)
                    {
                        if (h.transform.GetComponent<EnvironmentTile>().PlayerCanReach)
                        {
                            tile = h.transform.GetComponent<EnvironmentTile>();
                            break;
                        }
                    }
                }

                if      (tile == null)          Debug.Log("No Tile");
                else if (!tile.PlayerCanReach)  Debug.Log("Player can't reach");

                // Move the character to the selected tile
                if (tile != null && tile.PlayerCanReach)
                {
                    GetComponent<CharacterManager>().GetActiveUnit().CurrentPosition.playerOcupied = null;
                    MovementControls();
                    List<EnvironmentTile> route = map.Solve(GetComponent<CharacterManager>().GetActiveUnit().CurrentPosition, tile);
                    GetComponent<CharacterManager>().GetActiveUnit().GoTo(route);
                    GetComponent<CharacterManager>().UpdateUI();
                    tile.playerOcupied = GetComponent<CharacterManager>().GetActiveUnit();
                }
            }
        }
    }

    public void MovementControls()
    {
        GetComponent<CharacterManager>().ClearUI();

        // Return if character is out of AP
        if (GetComponent<CharacterManager>().GetActiveUnit().actionPoints <= 0)
        {
            enableMovement = false;
            Debug.Log("NO ACTION POINTS REMAINING");
            return;
        }

        if (GetComponent<CharacterManager>().GetActiveUnit().state == Character.States.TurnInProgress)
        {
            enableMovement = false;
            Debug.Log("Action In Progress");
            return;
        }

        enableMovement = !enableMovement;

        if  (enableMovement)    map.ShowMoveMaterial(GetComponent<CharacterManager>().GetActiveUnit().CurrentPosition, GetComponent<CharacterManager>().GetActiveUnit().movementPerAP, false);
        else                    map.ShowMoveMaterial(GetComponent<CharacterManager>().GetActiveUnit().CurrentPosition, GetComponent<CharacterManager>().GetActiveUnit().movementPerAP, true);

        Debug.Log(enableMovement);
    }

}
