﻿/*
    
    --------------------------------------------------------------------------------------

    . Remove unused content         X
    . organise project              X
    . create texture                X
    . replace current tiles         X


    BEFORE ADDING ANYTHING NEW

    . ADD OLD CONTENT BACK (CAMERA UI)                              X
    . FIX BUGS
    . ADD NPCS
        . Once activated will dash to the nearest extraction point  X
        . Stand next to them to activate                            X
        . visuals of current state                                  X
        . Have camera focus on them during turn                     X

    . ADD GAME START AND END
        . (FEATURE) EXTRATION ZONE - TAKES TWO TURNS TO SET UP      X
        . Main Menu                                                 X
            . Character Select and naming                           X
        . End Screen                                                X
            . Stats on performance                                  X
            
    . Randomness
        . Have a chance of prefab spawning on tile                  X
        . be able to mark tiles as inore randoness                  X
        . tag randomness                        
            . tree zone     (Randomise tress, scl/rot)              -
            . woodland      (rocks, plants, logs)                   -
            . industrial    (boxes, plattes, barrels)               -

    . CLEAN UP CODE, COMMENT AND MAKE EVERYTHING NICE

    . Add end if:                                                   X
        . All enemies killed                                        X
        . All Npc's and units haved escaped or died                 X

    . Zombies attack npcs
    . Zombie vision / Pack disscovery

    . Vision
    . Shooting blocking through walls, and blocks

    . ADD ABILITIES
    . DIFFERENT CHARACTERS
    . DIFFERENT ENEMY

    . OBJECT HIDING

    . Make it so vision test is performed during movement (player and render camera)
    . Implement shooting
    . Pack Anoucments

    . Render Texture video feed post process effect

    --------------------------------------------------------------------------------------

    !!! FORMATTING !!!
        . UI Manager
        . Base class for Character/ Enemy/ NPC managers

    !!! FEATURES !!!
        . OVERWATCH
        . ABILITYS
            . Genade
            . HeadShot - 10% chance to instant kill
            . Dodge - Do not take damage from next meeele attack

        . Cover
        . SMART CHASE (IF 3/4 ZOMBIES CHASING UNIT HAVE ZOMBIE CHASE A DIFFERENT TARGET)         
        . Enemy off screen Turns
        . Fog of war for enemies

    !!! BUGS !!!
        . AI AGENTS NO VALID MOVE. Same with NPCs
        . Movement requiring double click to enable movement (bool) after ClearUI       X
        . Camera World movement
        . Characters staring in same location                                           X
        . Using world camera, error with input text in the character selection

    !!! POLISH !!!!
        . ACTION CAMERA WHEH MOVING AND SHOOTING.
        . Diagional movement
        . World Boundrys
        . opening doors

    !!! Sorting !!!
        . Sorting
        . Enemies cloeset to Target go first

    !!! DONE !!!
        . (FEATURE) UNIT UI.
        . (BUG)     CAMERA LOCK WHEN KILL ENEMY AND CAMERA TARGETTING UNIT.
        . (FEATURE) DEAD UNITS TURN INTO ZOMBIES.
        . (SORTING) ATTACK UI BY BEST CHANCE
        . (FEATURE) Tile Materials
        . (FEATURE) Tooltips
        . (FEATURE) Show path
        . (FEATURE) Manual world
        . (FEATURE) Height in map
        . (FEATURE) Boundrys and blocks
        . (FEATURE) ENEMY PACKS
 
*/
