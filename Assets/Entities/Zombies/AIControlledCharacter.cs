﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIControlledCharacter : Character
{
    public Pack         AIPack { get; set; }
    public AIManager    Manager { get; set; }
    public Environment  Map { get; set; }

    public Transform cameraHolder;

    public bool VisionTest()
    {
        Camera cam = GetComponentInChildren<Camera>();

        // If pack is visible look to kill NPCs
        if (AIPack.visability == Pack.PackVisabilityStatus.Visable)
        {
            foreach (Character npc in Manager.GetComponent<NPCManager>().NPCs)
            {
                if (AIPack.SightedNPCs.Contains(npc)) continue;

                if (npc.IsVisibleFrom(cam)) AIPack.SightedNPCs.Add(npc);
            }
        }

        // If inactive look for the player units
        if (AIPack.status == Pack.PackStatus.InActive)
        {
            foreach (Character playerUnits in Manager.GetComponent<CharacterManager>().GetUnits())
            {
                if (playerUnits.IsVisibleFrom(cam)) return true;
            }
        }

        return false;
    }

    public IEnumerator TakeEnemyTurn()
    {
        if (Manager.GetComponent<CharacterManager>().GetUnits().Count == 0) { state = Character.States.TurnTaken; yield break; }

        List<Character> targets = new List<Character>();

        // Get units to attack depending on state
        if (AIPack.status == Pack.PackStatus.Active)
        {
            List<Character> c = Manager.GetComponent<CharacterManager>().GetUnits();
            foreach (Character npc in AIPack.SightedNPCs) c.Add(npc);
            targets = c;
        }
        else if (AIPack.visability == Pack.PackVisabilityStatus.Visable)
        {
            targets = AIPack.SightedNPCs;
        }

        if (targets.Count > 0)
        {
            state = Character.States.Active;

            while (actionPoints > 0)
            {
                if (state == Character.States.Active)
                {
                    // Find the closest target that can be reached
                    AttackTarget targetDetails = FindClosest(targets);

                    // If a target was found that can be moved too
                    if (targetDetails.found)
                    {                        
                        int step = Map.StepDistance(CurrentPosition, targetDetails.tile);

                        if (step < 2)
                        {
                            // If next to target then attack
                            MeeleAttack(targetDetails.target);
                        }
                        else
                        {
                            // Move to target
                            CurrentPosition.playerOcupied = null;

                            // Enough AP to move straight to target 
                            if (targetDetails.route.Count <= movementPerAP)
                            {
                                GoTo(targetDetails.route);
                                targetDetails.route[targetDetails.route.Count - 1].playerOcupied = this;
                                CurrentPosition = targetDetails.route[targetDetails.route.Count - 1];
                            }
                            else
                            {
                                // Can only move part of the way to the target
                                List<EnvironmentTile> partPath = FindPartPath(targetDetails.tile);

                                if (partPath.Count == 0)
                                {
                                    actionPoints = 0;
                                    state = States.TurnTaken;
                                    yield break;
                                }
                                else
                                {
                                    GoTo(partPath);
                                    partPath[partPath.Count - 1].playerOcupied = this;
                                    CurrentPosition = targetDetails.route[partPath.Count - 1];
                                }
                            }
                        }
                    }
                    else
                    {
                        actionPoints = 0;
                        state        = States.TurnTaken;
                        yield break;
                    }
                }

                yield return 0;
            }
        }
        else if (AIPack.visability == Pack.PackVisabilityStatus.Visable)
        { 
            // If visible but not active move a tile in a random direction

            List<EnvironmentTile> shuffledList = CurrentPosition.Connections.OrderBy(x => Random.value).ToList();

            // Move in a random direction if possible
            foreach (EnvironmentTile tile in shuffledList)
            {
                if (tile.IsAccessible && tile.playerOcupied == null)
                {
                    List<EnvironmentTile> route   = Map.Solve(CurrentPosition, tile);
                    CurrentPosition.playerOcupied = null;

                    GoTo(route);
                    route[route.Count - 1].playerOcupied = this;
                    CurrentPosition = route[route.Count - 1];
                    break;
                }
            }          
        }

        state = Character.States.TurnTaken;
        yield break;
    }

    // Holds details about the target
    protected struct AttackTarget
    {
        public AttackTarget(Character _target, EnvironmentTile _tile, bool _found, List<EnvironmentTile> _route) { target = _target; tile = _tile; found = _found; route = _route; }

        public Character       target;
        public EnvironmentTile tile;
        public bool            found;
        public List<EnvironmentTile>  route;
    }

    // Find the cloeset viable target
    protected virtual AttackTarget FindClosest(List<Character> characterList)
    {
        AttackTarget closest = new AttackTarget(null, null, false, null);

        int count = 999;

        foreach (Character unit in characterList)
        {
            List<EnvironmentTile> routeCharater = Map.Solve(CurrentPosition, unit.CurrentPosition);

            if (routeCharater.Count < count)
            {
                int tilecCount = 999;

                foreach (EnvironmentTile tile in unit.CurrentPosition.Connections)
                {
                    if (tile.IsAccessible && (tile.playerOcupied == null || tile.playerOcupied.Equals(this)))
                    {
                        int stepCount = Map.StepDistance(CurrentPosition, tile);

                        if (stepCount < tilecCount)
                        {
                            closest     = new AttackTarget(unit, tile, true, Map.Solve(CurrentPosition, tile));
                            tilecCount  = stepCount;
                            count       = routeCharater.Count;

                            if (stepCount == 0) return closest;
                        }
                    }
                }
            }
        }

        return closest;
    }

    // Get as close to the target as possible
    public List<EnvironmentTile> FindPartPath(EnvironmentTile target)
    {
        List<EnvironmentTile> route    = Map.Solve(CurrentPosition, target);
        List<EnvironmentTile> newRoute = Map.Solve(CurrentPosition, route[movementPerAP - 1]);
        List<EnvironmentTile> partPath = new List<EnvironmentTile>();

        for (int i = 0; i < newRoute.Count; i++)
        {
            if  (newRoute[i].playerOcupied == null) partPath.Add(newRoute[i]);
        }

        return partPath;
    }

    public override void TakeDamage(int amount, Character dealer)
    {
        base.TakeDamage(amount, this);

        // Make pack active if unit is shot
        if (AIPack.status == Pack.PackStatus.InActive) StartCoroutine(AIPack.AnnouncePack(this));

        if (health <= 0)
        {
            if (dealer.GetType() == typeof(Soldier)) { dealer.actionPoints++; }

            AIPack.KillUnit(this);
        }
    }
}
