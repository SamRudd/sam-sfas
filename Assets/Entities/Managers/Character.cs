﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{ 
    public enum States { WaitingForTurn, Active, TurnTaken, TurnInProgress, Dead, }

    [Header("State")]
    public States state;
    private States tempState;

    [Header("MOVEMENT")]
    [SerializeField] protected float SingleNodeMoveTime = 0.5f;

    [Header("UI")]
    public GameObject UIobj;

    [Header ("AP")]
    public int startAP = 2;
    public int actionPoints = 2;
    public int movementPerAP = 5;

    [Header ("HP")]
    public int Starthealth = 100;
    public int health = 100;
    public GameObject DamageText;
    public Color damageTextColour;
    public Color damageTextCritColour;

    [Header ("Attack")]
    public int damage = 60;
    public int critMultipler = 2;
    public LayerMask BlockingLayer;
    public Renderer render;

    [Header("AUDIO")]
    public AudioSource hitNoise;
    public AudioSource deathNoise;

    public EnvironmentTile CurrentPosition { get; set; }

    public int Health { get { return health; } set { if (health + value > Starthealth) health = Starthealth; else health += value; } } 

    // Abstract Functions
    public virtual void OnLMB() { }
    public virtual void OnRMB() { }
    public virtual void OnMMB() { }
    public virtual void OnHover() { }
    public virtual void OnHoverEnter() { }
    public virtual void OnHoverExit() { }
    public virtual void Init() { }

    public virtual void OnSightingEnter(Transform unit) { }
    public virtual void OnSighting(Transform unit) { }
    public virtual void OnSightingExit(Transform unit) { }

    private void Awake()
    {
        Init();
    }

    // Mouse Input Handling
    private void OnMouseOver()
    {
        if      (Input.GetMouseButtonDown(0))   OnLMB();
        else if (Input.GetMouseButtonDown(1))   OnRMB();
        else if (Input.GetMouseButtonDown(0))   OnMMB();
        else                                    OnHover();
    }
    private void OnMouseEnter() { OnHoverEnter();}
    public void OnMouseExit() { OnHoverExit(); }

    private IEnumerator DoMove(Vector3 position, Vector3 destination)
    {
        // Move between the two specified positions over the specified amount of time
        if (position != destination)
        {
            transform.eulerAngles = new Vector3(transform.rotation.eulerAngles.x, Quaternion.LookRotation(destination - position, Vector3.up).eulerAngles.y, transform.rotation.eulerAngles.z);//Quaternion.LookRotation(destination - position, Vector3.up);

            Vector3 p = transform.position;
            float t   = 0.0f;

            while (t < SingleNodeMoveTime)
            {
                t += Time.deltaTime;
                p = Vector3.Lerp(position, destination, t / SingleNodeMoveTime);
                transform.position = p;
                yield return null;
            }
        }
    }

    private IEnumerator DoGoTo(List<EnvironmentTile> route)
    {
        // Move through each tile in the given route
        if (route != null)
        {
            tempState = state;
            state = States.TurnInProgress;

            Vector3 position = CurrentPosition.Position;
            for (int count = 0; count < route.Count; ++count)
            {
                //obj.transform.position = tile.Position + (Vector3.up * tile.transform.localScale.y) - new Vector3(4.0f, 0.5f, 4.0f);

                Vector3 next    = route[count].Position + (Vector3.up * route[count].transform.localScale.y) - new Vector3(0.0f, 1.5f, 0.0f);
                yield return DoMove(position, next);
                CurrentPosition = route[count];
                position        = next;
            }

            transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, transform.eulerAngles.z);
            state = tempState;
            actionPoints--;
            
        }
    }

    public void GoTo(List<EnvironmentTile> route)
    {
        // Clear all coroutines before starting the new route so 
        // that clicks can interupt any current route animation
        StopAllCoroutines();
        StartCoroutine(DoGoTo(route));
    }

    // This function should never be called on a dead character
    public virtual void ResetCharacter()
    {
        actionPoints = startAP;
        if (state != States.Dead) state = States.WaitingForTurn;
    }

    public virtual void Attack(Character enemy)
    {
        if (actionPoints > 0)
        {
            Camera.main.GetComponent<CameraMovement>().StopCharacterLookAt();
            actionPoints = 0;
        }
        else
        {
            Debug.Log("OUT OF ACTION POINTS");
        }
    }

    public virtual void MeeleAttack(Character enemy)
    {
        if (actionPoints > 0)
        {
            tempState = state;
            state = States.TurnInProgress;

            enemy.TakeDamage(30, this);
            actionPoints--;

            state = tempState;
        }
        else
        {
            Debug.Log("OUT OF ACTION POINTS");
        }
    }

    public virtual void TakeDamage(int amount, Character dealer)
    {
        if (hitNoise != null) hitNoise.Play();

        health -= amount;
        if (DamageText != null) ShowDamageText(amount.ToString());

        if (health <= 0)
        {
            state = States.Dead;
            if (hitNoise != null) deathNoise.Play();
        }
    }

    private void ShowDamageText(string amount, bool crit = false)
    {
        string displayText  = "-" + amount;
        Color displayColour = damageTextColour;

        if (crit) { displayText = displayText.Insert(0, "Crit: "); displayColour = damageTextCritColour; }

        GameObject go = Instantiate(DamageText, transform.position, Quaternion.identity, transform);
        go.GetComponent<UIDamageText>().Init(displayText, displayColour);
    }   



    public bool IsVisibleFrom(Camera camera)
    {
        Plane[] planes      = GeometryUtility.CalculateFrustumPlanes(camera);

        Vector3 forward     = camera.transform.forward;
        Vector3 toTarget    = (transform.position - camera.transform.position).normalized;
        
        float angle = Vector3.SignedAngle(forward, toTarget, Vector3.up);

        if (angle > 100.0f) return false; 

        if (GeometryUtility.TestPlanesAABB(planes, render.bounds))
        {
            if (!Physics.Linecast(camera.transform.position, render.transform.position, BlockingLayer)) { Debug.DrawLine(camera.transform.position, render.transform.position, Color.black, 60.0f); return true; }

            Vector3 boundPoint1 = render.bounds.min;
            Vector3 boundPoint2 = render.bounds.max;

            if (!Physics.Linecast(camera.transform.position, render.bounds.min, BlockingLayer)) { Debug.DrawLine(camera.transform.position, render.bounds.min, Color.black, 60.0f); return true;}
            if (!Physics.Linecast(camera.transform.position, render.bounds.max, BlockingLayer)) { Debug.DrawLine(camera.transform.position, render.bounds.max, Color.black, 60.0f); return true; }


            //Vector3 boundPoint1 = render.bounds.min;
            //Vector3 boundPoint2 = render.bounds.max;

            //int raycastcount = 0;

            //if (!Physics.Linecast(camera.transform.position, render.bounds.min, BlockingLayer))                                         {raycastcount++;/*Debug.DrawLine(camera.transform.position, render.bounds.min, Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, render.bounds.max, BlockingLayer))                                         {raycastcount++;/*Debug.DrawLine(camera.transform.position, render.bounds.max, Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z), BlockingLayer))  {raycastcount++;/*Debug.DrawLine(camera.transform.position, new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z), Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z), BlockingLayer))  {raycastcount++;/*Debug.DrawLine(camera.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z), Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z), BlockingLayer))  {raycastcount++;/*Debug.DrawLine(camera.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z), Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z), BlockingLayer))  {raycastcount++;/*Debug.DrawLine(camera.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z), Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z), BlockingLayer))  {raycastcount++;/*Debug.DrawLine(camera.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z), Color.black, 60.0f);*/}
            //if (!Physics.Linecast(camera.transform.position, new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z), BlockingLayer))  {raycastcount++;/*Debug.DrawLine(camera.transform.position, new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z), Color.black, 60.0f);*/ }

            //if (raycastcount >= 2) return true;

            return false;
        }

        return false;
    }
}
