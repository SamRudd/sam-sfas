﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentTile : MonoBehaviour
{
    [Header("RANDOM GENERATION")]
    [SerializeField] private bool randomness = false;
    [SerializeField] private LayerMask layerMaskBoundry;
    [SerializeField] private LayerMask layerMaskBlock;

    // Tile Data
    public List<EnvironmentTile> Connections { get; set; }
    public EnvironmentTile Parent { get; set; }
    public Vector3 Position { get; set; }
    public float Global { get; set; }
    public float Local { get; set; }
    public bool Visited { get; set; }
    public bool IsAccessible { get; set; }
    public Vector2Int Index { get; set; }
    public bool UseRandomness { get { return randomness; } private set { } }

    // Player Movement
    public bool CanExtract { get; set; }
    public Character playerOcupied { get; set; }
    public bool PlayerCanReach { get; set; }

    // Blocking Data
    public bool BounryUp        { get; private set; }
    public bool BoundryDown     { get; private set; }
    public bool BoundryLeft     { get; private set; }
    public bool BoundryRight    { get; private set; }
    public bool TileBlocked     { get; private set; }

    // Check if any objects on the tile are blocking access to connected tiles, if so then set movement to that tile as blocked
    public void SetupBoundrys()
    {
        RaycastHit hit;
        const float boundryMod = 4.5f;

        // Check for center blocking object
        if (Physics.Raycast(Position - Vector3.up, Vector3.up, out hit, Mathf.Infinity, layerMaskBlock)) TileBlocked = true;

        // Check if boundries on the left, right, forward and backward
        if (Physics.Raycast(Position + Vector3.forward * boundryMod - Vector3.up, Vector3.up, out hit, Mathf.Infinity, layerMaskBoundry))   BounryUp     = true;
        if (Physics.Raycast(Position + Vector3.left * boundryMod - Vector3.up, Vector3.up, out hit, Mathf.Infinity, layerMaskBoundry))      BoundryLeft  = true;
        if (Physics.Raycast(Position + Vector3.right * boundryMod - Vector3.up, Vector3.up, out hit, Mathf.Infinity, layerMaskBoundry))     BoundryRight = true;
        if (Physics.Raycast(Position + -Vector3.forward * boundryMod - Vector3.up, Vector3.up, out hit, Mathf.Infinity, layerMaskBoundry))  BoundryDown  = true;       
    }

    public void MergeMeshes()
    {
        // Not yet Implemented
        // Combine all child meshes to this mesh
    }
}
