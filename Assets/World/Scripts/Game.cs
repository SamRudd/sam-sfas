﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField] private enum GameState { Menu, Game, Ended}
    [Header("GAME STATE")]
    [SerializeField] private GameState gameState;           // Tracks the state of the game

    [Header("CANVAS")]
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Canvas PlayerSelect;
    [SerializeField] private Canvas EndScreen;

    [Header("UNIT MANAGERS")]
    [SerializeField] private int frameUpdateCount = 10;     // The amount of frames which go by before the FrameUpdate is called on the managers
    private int framecount = 0;                             // Tracks the current frame for updating the FrameUpdates
    [SerializeField] private TurnManager[] turnManagers;    // Managers for the different units (player controlled, NPC, AI)

    private Environment mMap;                               // Holds the data about the game map
    private int managerTurnIndex = 0;                       // Tracks which manager is currently having their turn 
    public int Turn { get; private set; }                   // Tracks which turn the game is on

    void Start()
    {
        Turn = 1;
        mMap = GetComponentInChildren<Environment>(); 
        
        ShowMenu(true);
    }

    private void Update()
    {
        if (gameState == GameState.Game)
        {
            framecount++;

            // Managers StartTurn is called once then the managers Update is called every frame until its turn is done
            if      (turnManagers[managerTurnIndex].turnState == TurnManager.TurnState.WaitingForTurn) turnManagers[managerTurnIndex].StartTurn();
            else if (turnManagers[managerTurnIndex].turnState == TurnManager.TurnState.Active)         turnManagers[managerTurnIndex].UpdateManager();
            else if (turnManagers[managerTurnIndex].turnState == TurnManager.TurnState.TurnEnded)      managerTurnIndex++;

            // Check if the game has ended
            foreach (TurnManager turnMan in turnManagers) if (turnMan.IsCompleted()) EndGame();

            // Every manager has their FrameManager called every frameUpdateCount frames, used for vision and searching for targets
            if (framecount > frameUpdateCount)
            {
                foreach (TurnManager turnMan in turnManagers) turnMan.FrameManager();
                framecount = 0;
            }

            // If all managers have had their turn then reset them and begin again
            if (managerTurnIndex >= turnManagers.Length) ResetTurn();
        }
    }

    // Reset Managers and increment turn count
    private void ResetTurn()
    {
        Turn++; // Move to next frame

        // Reset managers
        managerTurnIndex = 0;
        foreach (TurnManager tm in turnManagers) tm.turnState = TurnManager.TurnState.WaitingForTurn;
    }

    // Move from menu to Character Selection
    public void ShowMenu(bool show)
    {
        if (Menu != null && PlayerSelect != null)
        {
            Menu.enabled         = show;
            PlayerSelect.enabled = !show;

            if (show)
            {
                gameState = GameState.Menu;
                mMap.CleanUpWorld();    
            }
            else
            {
                PlayerSelect.enabled = true;
            }
        }
    }

    // Shows end UI and passes data for end stats and sets state
    public void EndGame()
    {
        Hud.enabled         = false;
        EndScreen.enabled   = true;

        // Pass managers so the end stats can be shown
        EndScreen.transform.GetComponent<SetupStats>().SetStats(GetComponent<CharacterManager>(), GetComponent<NPCManager>(), GetComponent<AIManager>());        

        gameState = GameState.Ended;
    }

    // Hides character select canvas and shows HUD
    public void BeignGame()
    {
        if (Hud != null)
        {
            PlayerSelect.enabled = false;
            Hud.enabled          = true;
        }
    }

    // Generates the world 
    public void Generate()
    {
        mMap.GenerateWorld();
        InitManagers();
    }

    // Starts up all the unit managers
    private void InitManagers()
    {
        gameState = GameState.Game;

        for (int i = turnManagers.Length - 1; i >= 0; i--) turnManagers[i].Init();
    }

    // Reloads the game
    public void ReloadMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Exits the game
    public void Exit()
    {
        #if !UNITY_EDITOR
                Application.Quit();
        #endif
    }
}
