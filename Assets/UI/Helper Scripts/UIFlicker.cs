﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFlicker : MonoBehaviour
{
    // Place on Text object to move the colour alpha between to values to create a flicker/ pulsing look

    [SerializeField] private float minAlpha = .2f;  // The minimum amount the text alpha will go down too
    [SerializeField] private float speed = 1.0f;

    private Text    text;
    private bool    countUp = false;    // Count up or down
    private float   alpha   = 1.0f;     // Tracks current alpha of colour

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // Count up or down clamping between the min and max
        if  (countUp)   alpha = Mathf.Clamp(alpha + (Time.deltaTime * speed), minAlpha, 1.0f);
        else            alpha = Mathf.Clamp(alpha - (Time.deltaTime * speed), minAlpha, 1.0f);

        // Check for state change
        if (alpha >= 1.0f || alpha <= minAlpha) countUp = !countUp;

        // Set colour
        text.color = new Color(text.color.a, text.color.g, text.color.b, alpha);
    }
}
