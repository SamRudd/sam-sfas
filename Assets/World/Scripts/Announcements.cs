﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Announcements : MonoBehaviour
{
    /*
     *  DISPLAY TEXT AND PLAY SOUND WHEN A SUBSTANTIAL EVENT HAPPENS   
    */

    [SerializeField] private Text       spottedTxt, activetxt, deathTxt;
    [SerializeField] private AudioClip  spottedSound, activeSound;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Show announcement text and play audio
    public void SpottedPack()
    {
        audioSource.Stop();
        audioSource.clip = spottedSound;
        audioSource.Play();

        StartCoroutine(FadeInAndOutText(2.0f, spottedTxt));
    }

    // Show announcement text and play audio
    public void PackActive()
    {
        audioSource.Stop();
        audioSource.clip = activeSound;
        audioSource.Play();

        StartCoroutine(FadeInAndOutText(2.0f, activetxt));
    }

    // Show announcement text
    public void NPCDeath()
    {
        StartCoroutine(FadeInAndOutText(2.0f, deathTxt));
    }

    // Slowly fade in text over time from transparent
    IEnumerator FadeTextToFullAlpha(float t, Text Title)
    {
        Title.color = new Color(Title.color.r, Title.color.g, Title.color.b, 0);
        while (Title.color.a < 1.0f)
        {
            Title.color = new Color(Title.color.r, Title.color.g, Title.color.b, Title.color.a + (Time.deltaTime / t));
            yield return null;
        }
    }

    // Slowly fade out text over time from opaque
    IEnumerator FadeTextToZeroAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
        while (i.color.a > 0.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
            yield return null;
        }
    }

    // Will slowly fade in text then after specified time fade it out again
    IEnumerator FadeInAndOutText(float time, Text text)
    {
        StartCoroutine(FadeTextToFullAlpha(1.0f, text));

        yield return new WaitForSeconds(time);

        StartCoroutine(FadeTextToZeroAlpha(1.0f, text));
    }
}
