﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawn : MonoBehaviour
{
    [SerializeField] private GameObject npcObj;
    [SerializeField] private Environment map;

    public NPC Spawn()
    {
        // Find the starting position
        int startX = (int)(transform.position.x / map.TileSize);
        int startY = (int)(transform.position.z / map.TileSize);

        // Spawn the NPC
        NPC npc             = Instantiate(npcObj, map.GetTile(startX, startY).Position + (Vector3.up * map.GetTile(startX, startY).transform.localScale.y) - new Vector3(0.0f, 1.5f, 0.0f), Quaternion.identity).GetComponent<NPC>();
        npc.mMap            = map;
        npc.CurrentPosition = map.GetTile(startX, startY);
        npc.CurrentPosition.playerOcupied = npc;

        return npc;
    }

    public void End()
    {
        Destroy(this.gameObject);
    }
}
