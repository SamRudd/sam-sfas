﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSelection : MonoBehaviour
{
    [SerializeField] private Material visible, selected;
    public Environment environment;
    public EnvironmentTile tile;
    [SerializeField] private LayerMask mask;

    private bool hovered = false;

    public void Enter()
    {
        GetComponent<Renderer>().material = selected;
        environment.DrawPath(tile);
    }

    public void Exit()
    {
        GetComponent<Renderer>().material = visible;
        environment.DeletePath();
    }


    // This is not the correct method but IPointer stopped working
    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000.0f, mask))
        {
            if (hit.transform.gameObject.Equals(gameObject))
            {
                if (!hovered)
                {
                    hovered = true;
                    Enter();
                }
            }
        }
        else
        {
            if (hovered)
            {
                hovered = false;
                Exit();
            }
        }
    }
}
