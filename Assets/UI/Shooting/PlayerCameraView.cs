﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerCameraView : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [SerializeField] private CharacterManager cm;
    [SerializeField] private float dragSpeed = 5.0f;    // Speed of the drag

    private Vector2     origin;         // Point of drag
    private Vector2     direction;      // Drag movement direction
    private bool        touched;        // Has the render texture been clicked on
    private int         pointerID;      // Mouse details
    private RawImage    img;            // The render image

    private void Awake()
    {
        img = GetComponent<RawImage>();
    }

    // Render the new camera
    public void TargetChanged()
    {
        img.texture = cm.GetActiveUnit().cam.targetTexture;
    }

    // MOUSE INPUT
    public void OnPointerDown(PointerEventData data)
    {
        // Only set once per press
        if (!touched)
        {
            touched     = true;
            pointerID   = data.pointerId;
            origin      = data.position;
        }
    }

    // If drag occurs then rotate the camera by the same amount
    public void OnDrag(PointerEventData data)
    {
        if (data.pointerId == pointerID)
        {
            // Work out the direction the mouse is dragged in
            Vector2 currentPosition = data.position;
            Vector2 directionRaw    = currentPosition - origin;
            direction               = (directionRaw * Time.deltaTime);

            // Get Unit
            Transform unit  = cm.GetActiveUnit().transform;
            Camera cam      = cm.GetActiveUnit().cam;

            direction *= dragSpeed;

            // Roate the unit the same direction as drag
            unit.transform.eulerAngles = new Vector3(unit.transform.eulerAngles.x, unit.transform.eulerAngles.y + direction.x, unit.transform.eulerAngles.z);
            cam.transform.eulerAngles  = new Vector3(cam.transform.eulerAngles.x - direction.y, cam.transform.eulerAngles.y, cam.transform.eulerAngles.z);

            // Recalulate the attack UI
            cm.UpdateAttackUI();

            origin = data.position;
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (data.pointerId == pointerID)
        {
            direction = Vector2.zero;
            touched = false;
        }
    }

}
