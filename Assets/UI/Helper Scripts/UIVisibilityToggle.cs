﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIVisibilityToggle : MonoBehaviour
{
    public void Toggle()
    {
        GetComponent<Canvas>().enabled = !GetComponent<Canvas>().enabled;
    }

}
