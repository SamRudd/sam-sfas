﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : PlayerControledCharacter
{
    public override void Ability()
    {
        manager.CritMultipler = true;
        manager.BeginAttack();
    }

    // Override the Accuracy function because negative trait requires ajustments to accuracy
    public override float Accuracy(AIControlledCharacter target)
    {
        int raycastcount = 0;

        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);

        if (GeometryUtility.TestPlanesAABB(planes, target.GetComponent<BoxCollider>().bounds))
        {

            foreach (SkinnedMeshRenderer rend in target.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                Vector3 boundPoint1 = rend.bounds.min;
                Vector3 boundPoint2 = rend.bounds.max;

                if (!Physics.Linecast(cam.transform.position, rend.bounds.min, BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, rend.bounds.max, BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z), BlockingLayer))
                {
                    Debug.DrawLine(cam.transform.position, rend.transform.position);
                    raycastcount++;
                }
            }
        }

        float accuracy = 0.0f;
        float distance = Vector3.Distance(target.transform.position, cam.transform.position);

        if (distance <= 15.0f) return 20.0f; 

        if (raycastcount == 0) accuracy = 0.0f;

        if (raycastcount == 1) accuracy = 10.0f;

        if (raycastcount == 2) accuracy = 25.0f;

        if (raycastcount == 3) accuracy = 33.0f;

        if (raycastcount >= 4) accuracy = 50.0f;

        if      (distance < 20.0f)  accuracy *= 2.0f;
        else if (distance < 50.0f)  accuracy *= 1.6f;  
        else if (distance < 70.0f)  accuracy *= 1.3f;
        else if (distance < 100.0f) accuracy *= 1.0f;
        else if (distance < 120.0f) accuracy *= 0.75f;
        else if (distance < 130.0f) accuracy *= 0.5f;
        else                        accuracy = 0.0f;

        return accuracy;
    }
}
