﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControledCharacter : Character
{
    public CharacterManager manager;
    public Camera cam;
    public CharacterData characterData;

    public bool hunkered = false;

    public virtual void Ability()
    {

    }

    public override void OnLMB()
    {
        manager.SetActiveCharacter(this);
    }

    public override void ResetCharacter()
    {
        base.ResetCharacter();
        hunkered = false;
    }

    public void HunkerDown()
    {
        hunkered = true;
        actionPoints = 0;
    }

    public override void TakeDamage(int amount, Character dealer)
    {
        if (hunkered)
        {
            amount = Mathf.RoundToInt(amount / 2);
        }

        base.TakeDamage(amount, this);

        if (health <= 0)
        {
            manager.KillUnit(this);
        }
    }

    public void UIShoot(bool hit, bool crit)
    {
        actionPoints = 0;

        if      (crit)  en.TakeDamage(75, this);
        else if (hit)   en.TakeDamage(damage, this);

    }

    private Character en;

    public override void Attack(Character enemy)
    {
        if (actionPoints > 0)
        {
            Camera.main.GetComponent<CameraMovement>().StopCharacterLookAt();
            var lookPos = enemy.transform.position - transform.position;
            lookPos.y = 0;
            transform.rotation = Quaternion.LookRotation(lookPos);
            cam.transform.LookAt(enemy.transform.position + Vector3.up * 6.0f);
            en = enemy;

           // mActionPoints = 0;
        }
        else
        {
            Debug.Log("OUT OF ACTION POINTS");
        }
    }

    public struct EnemyVisibility
    {
        public EnemyVisibility(AIControlledCharacter ai, float acc) { character = ai; accuracy = acc; }

        public AIControlledCharacter   character;
        public float accuracy;
    }

    public EnemyVisibility[] GetAllVisible()
    {
        List<EnemyVisibility> enemies = new List<EnemyVisibility>();

        foreach (AIControlledCharacter ai in manager.GetComponent<AIManager>().GetAllUnits())
        {
            float acc = Accuracy(ai);
            if (acc > 0.0f)
            {
                float accuracy = acc;
                enemies.Add(new EnemyVisibility(ai, accuracy));
            }
        }

        return enemies.ToArray();
    }

    public virtual float Accuracy(AIControlledCharacter target)
    {
        int raycastcount = 0;

        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(cam);

        if (GeometryUtility.TestPlanesAABB(planes, target.GetComponent<BoxCollider>().bounds))
        {

            foreach (SkinnedMeshRenderer rend in target.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                Vector3 boundPoint1 = rend.bounds.min;
                Vector3 boundPoint2 = rend.bounds.max;

                if (!Physics.Linecast(cam.transform.position, rend.bounds.min, BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, rend.bounds.max, BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z), BlockingLayer) ||
                    !Physics.Linecast(cam.transform.position, new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z), BlockingLayer))
                {
                    Debug.DrawLine(cam.transform.position, rend.transform.position);
                    raycastcount++;
                }
            }
        }

        float accuracy = 0.0f;
        float distance = Vector3.Distance(target.transform.position, cam.transform.position);

        if (raycastcount == 0) accuracy = 0.0f;

        if (raycastcount == 1) accuracy = 10.0f;

        if (raycastcount == 2) accuracy = 25.0f;

        if (raycastcount == 3) accuracy = 33.0f;

        if (raycastcount >= 4) accuracy = 50.0f;

        if (distance < 20.0f)
        {
            accuracy *= 2.0f;
        }
        else if (distance < 50.0f)
        {
            accuracy *= 1.6f;
        }
        else if (distance < 70.0f)
        {
            accuracy *= 1.3f;
        }
        else if (distance < 100.0f)
        {
            accuracy *= 1.0f;
        }
        else if (distance < 120.0f)
        {
            accuracy *= 0.75f;
        }
        else if (distance < 130.0f)
        {
            accuracy *= 0.5f;
        }
        else
        {
            accuracy = 0.0f;
        }

        return accuracy;
    }
}
